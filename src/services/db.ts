import Employee from "../types/Employee";
import EmployeeEntry from "../types/EmployeeEntry";
import Note from "../types/Notes";
import RTTPaid from "../types/RTTPaid";

export const getProvider = (databaseName: string) => {
  const { remote } = window.require("electron");  
  return remote.getGlobal(databaseName);
};

export const addEmployee = async (employee: Employee) => {
  const db = getProvider("employeeDb");  
  const res = await db.create(employee);
  return {...res, id: res._id}
};

export const getAllEmployee = async () => {
  const db = getProvider("employeeDb");
  const employees = await db.readAll();
  const dataAugmented = employees.map((e: any) => {
    e.id = e._id
    return e
  })
  return dataAugmented
};

export const getEmployeeByName = async (firstName: string, lastName: string) => {
  const db = getProvider("employeeDb");
  const employee = await db.getByNames(firstName, lastName)
  const dataAugmented = {...employee, id: employee._id}
  return dataAugmented
}

export const updateEmployee = async (newEmployee: Employee) => {
  const db = getProvider("employeeDb");
  await db.update(newEmployee)
}


/******************/
/** EmployeeEntry */

export const addRecord = async (record: EmployeeEntry) => {
  const db = getProvider("employeeEntryDb")
  return db.create(record)
}

export const getDailyRecord = async (date: Date) => {
  const db = getProvider("employeeEntryDb")
  const data = await db.getDailyRecord(date)
  const dataAugmented = data.map((e: any) => {
    e.id = e._id
    return e;
  })
  return dataAugmented;
}

export const getRecords = async (): Promise<EmployeeEntry[]> => {
  const db = getProvider("employeeEntryDb")
  const data = await db.getRecords()
  const dataAugmented = data.map((e: any) => {
    e.id = e._id;
    return e
  })
  return dataAugmented;
}

export const updateEmployeeRecord = async (employeeEntryId: string, data: EmployeeEntry) => {
  const db = getProvider("employeeEntryDb")
  db.update(employeeEntryId, data);
}

export const removeEmployeeRecord = async (employeeEntryId: string) => {
  const db = getProvider("employeeEntryDb")
  db.delete(employeeEntryId)
}

/******************/
/** Notes */

export const getNotes = async (): Promise<Note[]> => {
  const db = getProvider("notesDb")
  const data = await db.getAll()
  return data.map((e: any) => ({...e, id: e._id}))
}

export const addNote = async (note: Note) => {
  const db = getProvider("notesDb")
  return db.create(note)
}

export const updateNote = async (noteId: string, data: Note) => {
  const db = getProvider("notesDb")
  db.update(noteId, data)
}

export const removeNote = async (noteId: string) => {
  const db = getProvider("notesDb")
  db.delete(noteId)
} 

/******************/
/** RTT Paids */

export const addRttPaid = async (rttPaid: RTTPaid) => {
  const db = getProvider("rttPaidDb")
  const res = await db.create(rttPaid)
  return {...res, id: res._id}
}

export const removeRttPaid = async (rttPaidId: string) => {
  const db = getProvider("rttPaidDb")
  db.delete(rttPaidId)
}

export const getRttPaids = async () => {
  const db = getProvider("rttPaidDb")
  const rttPaids = await db.getAll();
  const dataAugmented = rttPaids.map((e: any) => {
    e.id = e._id
    return e
  })
  return dataAugmented
}