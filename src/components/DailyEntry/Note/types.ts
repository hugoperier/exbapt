import { createStyles, WithStyles } from "@material-ui/core";
import Note from "../../../types/Notes";

export interface INotesProps extends WithStyles<typeof NotesStyle> {
    date: Date
}

export interface INotesState {
    saveStatusMessage: string;
    notes: Note[]
    note: Note | undefined
    timer: ReturnType<typeof setTimeout> | undefined
}

export const NotesStyle = () =>
    createStyles({

    })