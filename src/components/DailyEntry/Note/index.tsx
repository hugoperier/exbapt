import { TextField, withStyles } from "@material-ui/core";
import moment from "moment";
import React from "react";
import { addNote, getNotes, updateNote } from "../../../services/db";
import Note from "../../../types/Notes";
import { INotesProps, INotesState, NotesStyle } from "./types";

class Notes extends React.PureComponent<INotesProps, INotesState> {
    constructor(props: INotesProps) {
        super(props)
        this.state = {
            saveStatusMessage: "",
            notes: [],
            note: undefined,
            timer: undefined
        }
    }

    componentDidMount = async () => {
        const { date } = this.props

        const notes = await getNotes()
        const note = notes.find(e => moment(e.date).format('yyyy-MM-DD') === moment(date).format('yyyy-MM-DD'))
        this.setState({ notes, note })
    }

    componentDidUpdate = (oldProps: INotesProps) => {
        const { date } = this.props
        const { notes } = this.state

        if (oldProps.date !== date) {
            const note = notes.find(e => moment(e.date).format('yyyy-MM-DD') === moment(date).format('yyyy-MM-DD'))
            this.setState({ note, saveStatusMessage: "" })
        }
    }

    updateNoteInDb = async (note: Note) => {
        const { notes } = this.state

        updateNote(note.id, note)
        const noteIndex = notes.findIndex(n => n.id === note.id)
        if (noteIndex !== -1) {
            const newNotes = [
                ...notes.slice(0, noteIndex),
                Object.assign({}, notes[noteIndex], note),
                ...notes.slice(noteIndex + 1)
            ]
            this.setState({ notes: newNotes,  saveStatusMessage: "Sauvegardé ✅" })
        }
    }

    createNoteInDb = async (note: Note) => {
        const { notes } = this.state

        const newNotes = [...notes, note]
        addNote(note)
        this.setState({notes: newNotes, saveStatusMessage: "Sauvegardé ✅"})
    }

    onNoteUpdate = (e: any) => {
        const { timer, note, notes } = this.state
        const {date} = this.props

        if (timer)
            clearTimeout(timer)

        if (note && notes.find(e => e.id === note.id)) {
            const noteUpdated: Note = { ...note, content: e.target.value }
            const timer = setTimeout(() => { this.updateNoteInDb(noteUpdated) }, 700)
            this.setState({timer, note: noteUpdated, saveStatusMessage: "En cours"})
        }
        else {
            const noteCreated: Note = {
                id: "",
                date,
                content: e.target.value
            }
            const timer = setTimeout(() => { this.createNoteInDb(noteCreated) }, 700)
            this.setState({timer, note: noteCreated, saveStatusMessage: "En cours"})
        }
    }

    render() {
        const { saveStatusMessage, note } = this.state
        return (
            <TextField
                label="Notes"
                value={note === undefined ? "" : note.content}
                multiline
                fullWidth
                rows={4}
                onChange={this.onNoteUpdate}
                helperText={saveStatusMessage}
                variant="outlined"
            />
        )
    }
}

export default withStyles(NotesStyle)(Notes)