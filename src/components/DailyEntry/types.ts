import { createStyles, Theme, WithStyles } from "@material-ui/core";
import Employee from "../../types/Employee";
import EmployeeEntry from "../../types/EmployeeEntry";
import RTTPaid from "../../types/RTTPaid";

export interface IDailyEntryProps extends WithStyles<typeof DailyEntryStyle> {
  onTabChange: (name: string) => void;

  shouldRefresh: boolean
}

export interface IDailyEntryState {
  employees: Employee[];
  employeeRecords: EmployeeEntry[];
  rttPaids: RTTPaid[];
  recordInUpdate: EmployeeEntry | null
  date: Date
}

export const DailyEntryStyle = (theme: Theme) =>
  createStyles({
    title: {
      position: "relative",
      textAlign: "center",
      marginTop: 0,
      paddingTop: "20px",
    },
    gridAddRecord: {
      width: "30%",
      marginRight: "auto",
      marginLeft: "auto",
    },
    gridShowRecord: {
      width: "60%",
      marginRight: "auto",
      marginLeft: "auto",
    },
    noteComponent: {
      paddingTop: "20px",
    }
  });
