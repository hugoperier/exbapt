import React from "react";
import { IAddRecordProps, IAddRecordState, AddRecordStyle } from "./types";
import {
  Paper,
  withStyles,
  TextField,
  FormGroup,
  FormControlLabel,
  Checkbox,
  Button,
  Switch,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Collapse,
} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Employee from "../../../types/Employee";
import EmployeeEntry from "../../../types/EmployeeEntry";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import EmployeeExtraction from "../../../types/EmployeeExtraction";
import moment from "moment";
import absenceJSON from "../../../static/absences.json";

class AddRecord extends React.PureComponent<IAddRecordProps, IAddRecordState> {
  constructor(props: IAddRecordProps) {
    super(props);

    this.state = {
      record: {
        id: "",
        employeeId: "",
        employee: null,
        hoursWorked: 0,
        mg: 0,
        worksiteMeal: false,
        restaurant: false,
        absenceType: "",
        absenceHours: 0,
        publicHoliday: false,
        date: new Date(),
      },
      isAbsent: false,
      key: 0,
      employeeNameError: "",
      informationFormMissing: false,
    };
  }

  componentDidUpdate(oldProps: IAddRecordProps) {
    const { recordUpdate } = this.props;

    if (oldProps.recordUpdate !== recordUpdate) {
      if (recordUpdate !== null) {
        this.setState({
          record: recordUpdate,
          employeeNameError: "",
          isAbsent: recordUpdate.absenceType !== "",
        });
      } else {
        this.setState({
          record: {
            id: "",
            employeeId: "",
            employee: null,
            hoursWorked: 0,
            mg: 0,
            worksiteMeal: false,
            restaurant: false,
            absenceType: "",
            absenceHours: 0,
            publicHoliday: false,
            date: new Date(),
          },
          employeeNameError: "",
        });
      }
    }
  }

  submitRecordForm = () => {
    const { record } = this.state;
    const { records, date } = this.props;

    const dailyRecords = records.filter(
      (e) =>
        moment(e.date).format("yyyy-MM-DD") ===
        moment(date).format("yyyy-MM-DD")
    );
    if (
      record.employee &&
      !dailyRecords.some(
        (itm: EmployeeEntry) => itm.employeeId === record.employeeId
      )
    )
      this.setState(
        (prevstate) => ({
          record: {
            ...prevstate.record,
            date: date,
          },
        }),
        () => {
          this.props.addRecord(this.state.record);
          this.setState((prevstate) => ({
            record: {
              ...prevstate.record,
              employee: null,
              hoursWorked: 0,
              mg: 0,
              worksiteMeal: false,
              restaurant: false,
              absenceType: "",
              absenceHours: 0,
            },
            isAbsent: false,
            key: prevstate.key + 1,
          }));
        }
      );
    else {
      this.setState({
        informationFormMissing: true,
      });
    }
  };

  onSubmitUpdate = () => {
    const { recordUpdate } = this.props;
    const { record, employeeNameError } = this.state;

    if (record.employee && recordUpdate && employeeNameError === "") {
      this.props.updateRecord(recordUpdate, record);
      this.setState((prevstate) => ({
        record: {
          ...prevstate.record,
          employee: null,
          hoursWorked: 0,
          mg: 0,
          worksiteMeal: false,
          restaurant: false,
          absenceType: "",
          absenceHours: 0,
        },
        isAbsent: false,
        informationFormMissing: false,
        key: prevstate.key + 1,
      }));
    } else {
      this.setState({
        informationFormMissing: true,
      });
    }
  };

  onDeleteRecord = () => {
    const { recordUpdate } = this.props;

    if (recordUpdate) this.props.updateRecord(recordUpdate, null);
  };

  onRestaurantCheckBoxChange = () => {
    this.setState((prevstate) => ({
      record: {
        ...prevstate.record,
        restaurant: !prevstate.record.restaurant,
        worksiteMeal: false,
      },
      informationFormMissing: false,
    }));
  };

  onWsmCheckBoxChange = () => {
    this.setState((prevstate) => ({
      record: {
        ...prevstate.record,
        restaurant: false,
        worksiteMeal: !prevstate.record.worksiteMeal,
      },
      informationFormMissing: false,
    }));
  };

  onAutocompleteChange = (event: any, value: any) => {
    const { records, recordUpdate, date } = this.props;

    const dailyRecords = records.filter(
      (e) =>
        moment(e.date).format("yyy-MM-DD") === moment(date).format("yyy-MM-DD")
    );

    if (
      value &&
      dailyRecords.some((e) => e.employeeId === value._id) &&
      !(recordUpdate && recordUpdate.employeeId === value._id)
    )
      this.setState({
        employeeNameError: "Cette personne est déja inscrite aujourd'hui",
      });
    else
      this.setState((prevstate) => ({
        record: {
          ...prevstate.record,
          employee: value,
          employeeId: value === null ? prevstate.record.employeeId : value._id,
        },
        employeeNameError: "",
        informationFormMissing: false,
      }));
  };

  onHoursWorkedChange = (event: any) => {
    if (event.target.value) {
      const n = Number(event.target.value);
      this.setState((prevstate) => ({
        record: {
          ...prevstate.record,
          hoursWorked: n > 0 ? n : 0,
        },
        informationFormMissing: false,
      }));
    } else {
      this.setState((prevstate) => ({
        record: {
          ...prevstate.record,
          hoursWorked: 0,
        },
        informationFormMissing: false,
      }));
    }
  };

  onMgCheckBoxChange = (mg: number) => {
    this.setState((prevstate) => ({
      record: {
        ...prevstate.record,
        mg: prevstate.record.mg === mg ? 0 : mg,
      },
      informationFormMissing: false,
    }));
  };

  onAddEmployeeButtonClick = (e: any) => {
    e.stopPropagation();

    this.props.onOpenEmployeeGestion("gestionemployee");
  };

  onToggleSwitchAbsent = () => {
    const { isAbsent } = this.state;

    if (!isAbsent) {
      this.setState((prevstate) => ({
        record: {
          ...prevstate.record,
          absenceType: "",
          absenceHours: 7
        },
        isAbsent: true,
      }));
    } else {
      this.setState((prevstate) => ({
        record: {
          ...prevstate.record,
          absenceType: "",
          absenceHours: 0
        },
        isAbsent: false,
      }));
    }
  };

  handleDayoffChange = (event: any) => {
    this.setState((prevstate) => ({
      record: {
        ...prevstate.record,
        absenceType: event.target.value,
      },
    }));
  };

  render() {
    const {
      classes,
      records,
      employees,
      recordUpdate,
      onCancelUpdate,
      date,
      RTTPaids
    } = this.props;
    const {
      record,
      key,
      employeeNameError,
      informationFormMissing,
      isAbsent,
    } = this.state;
    const actualEntry =
      record !== null && record.employee !== null ? record.employee : null;

    const employeeRecordRtt = record.employee
      ? EmployeeExtraction.getCurrentRtt(records, record.employee, RTTPaids, date)
      : "";

    return (
      <div>
        <Paper elevation={3} className={classes.addRecordPaper}>
          {recordUpdate ? (
            <h2>Modifier une entrée</h2>
          ) : (
            <h2>Ajouter une entrée</h2>
          )}
          <Autocomplete
            className={classes.formContentRow}
            id="combo-box"
            noOptionsText="Aucun resultat"
            key={key}
            options={employees}
            onChange={this.onAutocompleteChange}
            popupIcon={
              <AddCircleOutlineIcon
                onClick={(e: any) => this.onAddEmployeeButtonClick(e)}
              />
            }
            getOptionLabel={(option: Employee) =>
              option.firstName + " " + option.lastName
            }
            value={actualEntry}
            style={{ width: 300 }}
            renderInput={(params) => (
              <TextField
                {...params}
                error={employeeNameError.length > 0}
                label={
                  employeeNameError.length
                    ? employeeNameError
                    : "Nom de l'employée"
                }
                variant="outlined"
                helperText={
                  record.employee !== null
                    ? `Solde RTT: ${employeeRecordRtt}`
                    : ""
                }
              />
            )}
          />

          <TextField
            className={classes.formContentRow}
            label="Heures Travaillés"
            type="number"
            value={record.hoursWorked}
            onChange={(e: any) => this.onHoursWorkedChange(e)}
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
            helperText={
              record.hoursWorked !== 7 && !isAbsent
                ? `${record.hoursWorked - 7 > 0 ? "+" : ""}${
                    record.hoursWorked - 7
                  } RTT`
                : ""
            }
          />
          <FormGroup row className={classes.formContentRow}>
            {[3, 4, 5, 6].map((value: number) => {
              return (
                <FormControlLabel
                  key={`${value}mg`}
                  control={
                    <Checkbox
                      checked={record.mg === value}
                      onChange={() => this.onMgCheckBoxChange(value)}
                      name={`${value}mg`}
                    />
                  }
                  labelPlacement="top"
                  label={`${value} mg`}
                />
              );
            })}
          </FormGroup>
          <FormGroup row className={classes.formContentRow}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={record.worksiteMeal}
                  onChange={this.onWsmCheckBoxChange}
                  name="worksitemeal"
                />
              }
              label="Repas Chantier"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={record.restaurant}
                  onChange={this.onRestaurantCheckBoxChange}
                  name="restaurant"
                />
              }
              label="Repas Restaurant"
            />
            <FormControlLabel
              className={classes.formContentRow}
              control={
                <Switch
                  checked={isAbsent}
                  onChange={this.onToggleSwitchAbsent}
                />
              }
              label="Absent ?"
            />
            <Collapse in={isAbsent}>
              <FormControl
                variant="outlined"
                className={classes.formContentRow}
              >
                <InputLabel id="demo-simple-select-outlined-label">
                  Type d'absence
                </InputLabel>
                <Select
                  value={record.absenceType}
                  onChange={this.handleDayoffChange}
                  labelWidth={120}
                >
                  {absenceJSON.map((absence) => (
                    <MenuItem key={absence.title} value={absence.value}>{absence.title}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <TextField
                className={classes.formContentRow}
                label="Heures d'absences"
                helperText={record.absenceHours === 7 ? "Journée entiere" : ""}
                type="number"
                value={record.absenceHours}
                onChange={(e: any) => {
                  const absenceHours =
                    +e.target.value >= 0 ? +e.target.value : 0;
                  this.setState((prevstate) => ({
                    record: {
                      ...prevstate.record,
                      absenceHours: absenceHours,
                    },
                  }));
                }}
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Collapse>
          </FormGroup>
          {informationFormMissing && (
            <p className={classes.errorMessage}>
              Veuillez remplir toute les informations
            </p>
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={
              recordUpdate === null
                ? this.submitRecordForm
                : this.onSubmitUpdate
            }
          >
            {recordUpdate === null ? "Ajouter" : "Modifier"}
          </Button>
          {recordUpdate && (
            <Button
              variant="contained"
              color="secondary"
              onClick={this.onDeleteRecord}
            >
              Supprimer
            </Button>
          )}
          {recordUpdate && (
            <Button
              variant="contained"
              color="secondary"
              onClick={() => {
                onCancelUpdate();
                this.setState({ isAbsent: false });
              }}
            >
              Annuler
            </Button>
          )}
        </Paper>
      </div>
    );
  }
}

export default withStyles(AddRecordStyle)(AddRecord);
