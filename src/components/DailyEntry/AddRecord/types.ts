import { createStyles, Theme, WithStyles } from "@material-ui/core";
import Employee from "../../../types/Employee";
import EmployeeEntry from "../../../types/EmployeeEntry";
import RTTPaid from "../../../types/RTTPaid";

export interface IAddRecordProps extends WithStyles<typeof AddRecordStyle> {
  employees: Employee[];
  records: EmployeeEntry[];
  RTTPaids: RTTPaid[];
  date: Date;
  onOpenEmployeeGestion: (tab: string) => void;
  onCancelUpdate: () => void
  
  addRecord: (record: EmployeeEntry) => void;

  updateRecord: (oldRecord: EmployeeEntry, newRecord: EmployeeEntry | null) => void
  recordUpdate: EmployeeEntry | null
}

export interface IAddRecordState {
  record: EmployeeEntry;
  key: number

  
  isAbsent: boolean;


  // Error Management
  informationFormMissing: boolean
  employeeNameError: string
}

export const AddRecordStyle = (theme: Theme) =>
  createStyles({
    addRecordPaper: {
      flexDirection: "column",
      display: "flex",
      textAlign: "center",
    },
    errorMessage:  {
      color: "red"
    },
    formContentRow: {
      width: "300px",
      paddingBottom: "20px",
      marginLeft: "auto",
      marginRight: "auto",
    },
  });
