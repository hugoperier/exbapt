import React, { Fragment, useEffect } from "react";
import {
  createStyles,
  makeStyles,
  Theme,
} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import EditIcon from "@material-ui/icons/Edit";
import Paper from "@material-ui/core/Paper";
import { Button, IconButton, TextField } from "@material-ui/core";
import EmployeeEntry from "../../../types/EmployeeEntry";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import moment from 'moment'
import absenceJSON from "../../../static/absences.json"

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

type Order = "asc" | "desc";

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key
): (
    a: { [key in Key]: number | string | boolean | any },
    b: { [key in Key]: number | string | boolean | any }
  ) => number {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

interface HeadCell {
  disablePadding: boolean;
  id: keyof EmployeeEntry;
  label: string;
  numeric: boolean;
}

const headCells: HeadCell[] = [
  { id: "employee", numeric: false, disablePadding: true, label: "Nom" },
  {
    id: "hoursWorked",
    numeric: false,
    disablePadding: false,
    label: "Heures Travaillés",
  },
  { id: "mg", numeric: false, disablePadding: false, label: "MG" },
  { id: "worksiteMeal", numeric: false, disablePadding: false, label: "Repas" },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  numSelected: number;
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof EmployeeEntry
  ) => void;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property: keyof EmployeeEntry) => (
    event: React.MouseEvent<unknown>
  ) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox"></TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      width: "200px",
      margin: "auto"
    },
    arrowRight: {
      float: 'left'
    },
    arrowLeft: {
      float: 'right'
    }
  })
);

interface EnhancedTableToolbarProps {
  date: Date;
  onDateUpdate: (newDate: Date) => void;
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const classes = useToolbarStyles();
  const [date, setDate] = React.useState(props.date)
  const handleDateChange = (event: any) => {
    changeDate({ newDate: event.target.value })
  }
  const changeDate = (opts?: { increment?: number, newDate?: Date }) => {
    if (opts?.increment) {
      const date = moment(props.date).add(opts.increment, 'days').toDate()
      setDate(date)
      props.onDateUpdate(date)
    }
    if (opts?.newDate) {
      props.onDateUpdate(opts.newDate)
      setDate(opts.newDate)
    }
  }

  return (
    <Fragment>
      <div>
        <IconButton className={classes.arrowRight} onClick={() => changeDate({ increment: -1 })}>
          <ArrowBackIcon />
        </IconButton>
        <IconButton className={classes.arrowLeft} onClick={() => changeDate({ increment: 1 })}>
          <ArrowForwardIcon />
        </IconButton>
      </div>
      <div className={classes.title}>
        <TextField
          type="date"
          size="small"
          value={moment(date).format('yyyy-MM-DD')}
          onChange={handleDateChange}
          variant="outlined"
          InputLabelProps={{
            shrink: true,
          }}
        />
      </div>
    </Fragment>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
    },
    paper: {
      width: "100%",
      marginBottom: theme.spacing(2),
    },
    table: {
      minWidth: 750,
    },
    visuallyHidden: {
      border: 0,
      clip: "rect(0 0 0 0)",
      height: 1,
      margin: -1,
      overflow: "hidden",
      padding: 0,
      position: "absolute",
      top: 20,
      width: 1,
    },
    redDot: {
      height: "25px",
      width: "25px",
      backgroundColor: "red",
      borderRadius: "50%",
      display: "inline-block",
    },
    greenDot: {
      height: "25px",
      width: "25px",
      backgroundColor: "green",
      borderRadius: "50%",
      display: "inline-block",
    },
  })
);

interface IEnhancedTableProps {
  data: EmployeeEntry[];
  setUpdateRecord: (row: EmployeeEntry) => void
  date: Date
  onDateUpdate: (newDate: Date) => void
}

export default function EnhancedTable(props: IEnhancedTableProps) {
  const { data, setUpdateRecord, date } = props;

  const classes = useStyles();
  const [order, setOrder] = React.useState<Order>("asc");
  const [orderBy, setOrderBy] = React.useState<keyof EmployeeEntry>("employee");
  const [selected] = React.useState<string[]>([]);
  const [page, setPage] = React.useState(0);
  const rowsPerPage = 10;
  const absenceTypes: any = absenceJSON.reduce((a, x) => ({ ...a, [x.value]: x.title }), {})

  useEffect(() => {
    setPage(0)
  }, [date])

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof EmployeeEntry
  ) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <EnhancedTableToolbar date={props.date} onDateUpdate={props.onDateUpdate} />
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={() => { }}
              onRequestSort={handleRequestSort}
              rowCount={data.length}
            />
            <TableBody>
              {stableSort(data, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={
                        row.employee === null
                          ? ""
                          : row.employee.firstName + row.employee.lastName
                      }
                    >
                      <TableCell padding="checkbox">
                        <Button onClick={() => setUpdateRecord(row)}>
                          <EditIcon />
                        </Button>
                      </TableCell>
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="none"
                      >
                        {row.employee?.firstName + " " + row.employee?.lastName}
                      </TableCell>
                      <TableCell align="left">
                        {row.absenceType === "" && row.hoursWorked}
                        {row.absenceHours === 0 && row.absenceType !== "" && 'Absence: ' + absenceTypes[row.absenceType]}
                        {row.absenceHours > 0 && row.absenceType !== "" && `Travaillé ${row.hoursWorked}h - ${absenceTypes[row.absenceType]} ${row.absenceHours}h`}
                      </TableCell>
                      <TableCell align="left">{row.mg > 0 && row.mg}</TableCell>
                      <TableCell align="left">
                        {row.restaurant && <span>Repas Restaurant</span>}
                        {row.worksiteMeal && <span>Repas Chantier</span>}
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to} sur ${count !== -1 ? count : ""}`
          }
          count={data.length}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={[10]}
          page={page}
          onChangePage={handleChangePage}
        />
      </Paper>
    </div>
  );
}
