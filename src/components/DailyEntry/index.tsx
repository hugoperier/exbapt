import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { IDailyEntryProps, IDailyEntryState, DailyEntryStyle } from "./types";
import { getAllEmployee, addRecord, updateEmployeeRecord, removeEmployeeRecord, getRecords, getRttPaids } from "../../services/db";
import { Button, Grid } from "@material-ui/core";
import AddRecord from "./AddRecord";
import EmployeeEntry from "../../types/EmployeeEntry";
import EnhancedTable from "./RecordTable";
import Employee from "../../types/Employee";
import moment from "moment";
import Notes from "./Note";

class DailyEntry extends React.PureComponent<
  IDailyEntryProps,
  IDailyEntryState
  > {
  constructor(props: IDailyEntryProps) {
    super(props);
    this.state = {
      employees: [],
      employeeRecords: [],
      rttPaids: [],
      recordInUpdate: null,
      date: new Date()
    };
  }

  componentDidUpdate = async (oldProps: IDailyEntryProps) => {
    if (!oldProps.shouldRefresh && this.props.shouldRefresh) {
      const employees = await getAllEmployee();
      const employeeRecords = await getRecords();
      const rttPaids = await getRttPaids()
      this.setState({ employees, employeeRecords, rttPaids });
    }
  }

  componentDidMount = async () => {
    const employees = await getAllEmployee();
    const employeeRecords = await getRecords();
    const rttPaids = await getRttPaids()
    this.setState({ employees, employeeRecords, rttPaids });
  };


  addRecord = async (record: EmployeeEntry) => {
    const { employeeRecords } = this.state;

    const res = await addRecord(record);
    record.id = res._id
    const newEmployeeRecords = [...employeeRecords, record];
    this.setState({ employeeRecords: newEmployeeRecords });
  };

  onCancelUpdate = () => {
    this.setState({
      recordInUpdate: null
    })
  }

  /*
  * If the new record is null it does mean that we have to remove it
  */
  onUpdateRecord = (oldRecord: EmployeeEntry, newRecord: EmployeeEntry | null) => {
    const { employeeRecords } = this.state

    const index = employeeRecords.findIndex(
      (record: EmployeeEntry) =>
        record.id === oldRecord.id
    )
    if (newRecord) {
      if (index !== -1) {
        this.setState({
          employeeRecords: [
            ...employeeRecords.slice(0, index),
            Object.assign({}, employeeRecords[index], newRecord),
            ...employeeRecords.slice(index + 1)
          ],
          recordInUpdate: null
        })
        updateEmployeeRecord(newRecord.id, newRecord)
      }
    }
    else {
      this.setState({
        employeeRecords: [
          ...employeeRecords.slice(0, index),
          ...employeeRecords.slice(index + 1)
        ],
        recordInUpdate: null
      })
      removeEmployeeRecord(oldRecord.id)
    }
  }

  setUpdateRecord = (row: EmployeeEntry) => {
    this.setState({ recordInUpdate: row })
  }

  onDateUpdate = (newDate: Date) => {
    this.setState({ date: new Date(newDate), recordInUpdate: null })
  }

  render() {
    const { classes, onTabChange } = this.props;
    const { employees, employeeRecords, recordInUpdate, date, rttPaids } = this.state;

    const employeesFiltered = employees.filter((emp: Employee) => emp.isActive);
    const dailyRecords = employeeRecords.filter(e => moment(e.date).format('yyyy-MM-DD') === moment(date).format('yyyy-MM-DD'))

    return (
      <div>
        <div className={classes.title}>
          <h1>Saisie Journaliere</h1>
        </div>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="flex-start"
        >
          <Grid item className={classes.gridAddRecord}>
            <AddRecord
              employees={employeesFiltered}
              addRecord={this.addRecord}
              RTTPaids={rttPaids}
              records={employeeRecords}
              onOpenEmployeeGestion={onTabChange}
              onCancelUpdate={this.onCancelUpdate}
              updateRecord={this.onUpdateRecord}
              recordUpdate={recordInUpdate}
              date={date}
            />
            <div className={classes.noteComponent}>
              <Notes date={date} />
            </div>
          </Grid>
          <Grid item className={classes.gridShowRecord}>
            <EnhancedTable onDateUpdate={this.onDateUpdate} date={date} data={dailyRecords} setUpdateRecord={this.setUpdateRecord} />
            <Button variant="contained" color="primary" onClick={() => onTabChange('extraction')}>Extractions</Button>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(DailyEntryStyle)(DailyEntry);
