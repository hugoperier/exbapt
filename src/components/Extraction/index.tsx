import React from "react"
import { withStyles } from "@material-ui/core/styles";
import { IExtractionProps, IExtractionState, ExtractionStyle } from "./types";
import { ExtractionType } from "../../types/ExtractionTypes";
import { Grid, Paper, FormControl, InputLabel, Select, MenuItem, Button, TextField } from "@material-ui/core";
import ExtractionTable from "./ExtractionTable";
import { getRecords } from "../../services/db";
import EmployeeExtraction from "../../types/EmployeeExtraction";
import EmployeeEntry from "../../types/EmployeeEntry";

class Extraction extends React.PureComponent<IExtractionProps, IExtractionState> {
    constructor(props: IExtractionProps) {
        super(props)
        this.state = {
            mode: ExtractionType.Daily,
            employeeExtractions: undefined,
            employeeExtractionsFiltered: undefined,
            date: new Date(),
            employeeQuery: ""
        }
    }

    componentDidMount = async () => {
        const { mode, date, employeeQuery } = this.state

        const groupBy = (arr: EmployeeEntry[], property: string) => {
            return arr.reduce((acc: any, cur: any) => {
                acc[cur[property]] = [...acc[cur[property]] || [], cur];
                return acc;
            }, {});
        }

        const employeeEntries = await getRecords()
        const uniqueEmployeeEntries = groupBy(employeeEntries, 'employeeId')
        const employeeExtractions: EmployeeExtraction[] = []
        for (const key in uniqueEmployeeEntries) {
            employeeExtractions.push(new EmployeeExtraction(uniqueEmployeeEntries[key], date, mode))
        }
        const employeeExtractionsFiltered = (employeeExtractions ? employeeExtractions.filter(e => (e.getTotalHoursWorked() > 0) || e.getAbsences() > 0)
            .filter(e => `${e.firstName.toLowerCase()}${e.lastName.toLowerCase()}`.includes(employeeQuery.toLowerCase())) : undefined)
        this.setState({ employeeExtractions, employeeExtractionsFiltered })
    }

    handleTypeExtractionChange = (e: any) => {
        const { employeeExtractions, employeeQuery } = this.state

        employeeExtractions?.forEach(employeeExtractions => employeeExtractions.mode = e.target.value)
        const employeeExtractionsFiltered = (employeeExtractions ? employeeExtractions.filter(e => (e.getTotalHoursWorked() > 0) || e.getAbsences() > 0)
            .filter(e => `${e.firstName.toLowerCase()}${e.lastName.toLowerCase()}`.includes(employeeQuery.toLowerCase())) : undefined)
        this.setState({ mode: e.target.value, employeeExtractionsFiltered })
    }

    settings = () => {
        const { classes, onTabChange } = this.props
        const { mode } = this.state

        return (
            <div style={{ textAlign: 'center' }}>
                <Paper elevation={3} className={classes.settingsPaper}>
                    <h3>Parametre de l'extraction</h3>
                    <FormControl className={classes.formContentRow}>
                        <InputLabel id="demo-simple-select-label">Type d'extraction</InputLabel>
                        <Select
                            value={mode}
                            onChange={this.handleTypeExtractionChange}
                        >
                            <MenuItem value={ExtractionType.Daily}>Journaliere</MenuItem>
                            <MenuItem value={ExtractionType.Monthly}>Mensuelle</MenuItem>
                            <MenuItem value={ExtractionType.Yearly}>Anuelle</MenuItem>
                        </Select>
                    </FormControl>
                    <FormControl className={classes.formContentRow}>
                        <TextField
                            label="Filtrer par employé"
                            value={this.state.employeeQuery}
                            onChange={(e: any) => this.onEmployeeQueryChange(e.target.value || "")}
                        />
                    </FormControl>
                </Paper>
                <Button className={classes.backButton} variant="contained" color="secondary" onClick={() => onTabChange("home")}>Retour</Button>
            </div>
        )
    }

    onEmployeeQueryChange = (employeeQuery: string) => {
        const { employeeExtractions } = this.state
        const employeeExtractionsFiltered = (employeeExtractions ? employeeExtractions.filter(e => (e.getTotalHoursWorked() > 0) || e.getAbsences() > 0)
            .filter(e => `${e.firstName.toLowerCase()}${e.lastName.toLowerCase()}`.includes(employeeQuery.toLowerCase())) : undefined)

        this.setState({ employeeQuery, employeeExtractionsFiltered })
    }

    onDateUpdate = (newDate: Date) => {
        const { employeeExtractions, employeeQuery } = this.state

        employeeExtractions?.forEach(employeeExtractions => employeeExtractions.date = newDate)
        const employeeExtractionsFiltered = (employeeExtractions ? employeeExtractions.filter(e => (e.getTotalHoursWorked() > 0) || e.getAbsences() > 0)
            .filter(e => `${e.firstName.toLowerCase()}${e.lastName.toLowerCase()}`.includes(employeeQuery.toLowerCase())) : undefined)

        this.setState({ date: newDate, employeeExtractionsFiltered })
    }

    render() {
        const { classes } = this.props
        const { date, mode, employeeExtractionsFiltered } = this.state

        return (
            <div>
                <div className={classes.title}>
                    <h1>Extractions</h1>
                </div>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="flex-start"
                >
                    <Grid item className={classes.settings}>
                        <this.settings />
                    </Grid>

                    <Grid item className={classes.records}>
                        {<ExtractionTable datas={employeeExtractionsFiltered} date={date} onDateUpdate={this.onDateUpdate} mode={mode} />}
                    </Grid>

                </Grid>
            </div>
        )
    }
}

export default withStyles(ExtractionStyle)(Extraction);
