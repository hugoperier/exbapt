import React, { Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import EmployeeExtraction from '../../../types/EmployeeExtraction';
import { CircularProgress, createStyles, Grid, TablePagination } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import moment from 'moment';
import { ExtractionType } from '../../../types/ExtractionTypes';
import { DatePicker } from '@material-ui/pickers/DatePicker/DatePicker';
import absenceJSON from "../../../static/absences.json"

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
  additionnalInfoDiv: {
    paddingBottom: "10px"
  }
});

function Row(props: { row: EmployeeExtraction }) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          {`${row.firstName} ${row.lastName}`}
        </TableCell>
        <TableCell align="right">{row.getTotalDaysWorked()}</TableCell>
        <TableCell align="right">{row.getTotalHoursWorked()}</TableCell>
        <TableCell align="right">0</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                Informations additionelles
              </Typography>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="flex-start"
              >
                <Grid item>
                  {[3, 4, 5, 6].map(mg => {
                    return (
                      <div className={classes.additionnalInfoDiv}>
                        <strong>{mg}mg: </strong>
                        {row.getNbMg(mg)}
                      </div>
                    )
                  })}
                </Grid>
                <Grid item style={{ marginLeft: "100px" }}>
                  <div className={classes.additionnalInfoDiv} title="Solde RTT disponible au début de la periode">
                    <strong>RTT Debut: </strong> {row.getRttByDate(true)}
                  </div>
                  <div className={classes.additionnalInfoDiv} title="Solde RTT disponible a la fin de la periode">
                    <strong>RTT Fin: </strong> {row.getRttByDate(false)}
                  </div>
                  <div className={classes.additionnalInfoDiv} title="RTT gagné durant la periode">
                    <strong>RTT Cumulé: </strong> {row.getAddedRTT()}
                  </div>
                  <div className={classes.additionnalInfoDiv} title="RTT utilisé durant la periode">
                    <strong>RTT Pris: </strong> {row.getTakenRTT()}
                  </div>
                </Grid>
                <Grid item style={{ marginLeft: "100px" }}>
                  {absenceJSON.map((e, i) => i < 4 && (
                    <div className={classes.additionnalInfoDiv}>
                      <strong>{e.title}: </strong> {row.getAbsences(e.value)}
                    </div>
                  ))}
                </Grid>
                <Grid item style={{ marginLeft: "100px" }}>
                  {absenceJSON.map((e, i) => i >= 4 && (
                    <div className={classes.additionnalInfoDiv}>
                      <strong>{e.title}: </strong> {row.getAbsences(e.value)}
                    </div>
                  ))}
                </Grid>
              </Grid>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

const useToolbarStyles = makeStyles(() =>
  createStyles({
    title: {
      width: "200px",
      margin: "auto"
    },
    arrowRight: {
      float: 'left'
    },
    arrowLeft: {
      float: 'right'
    }
  })
);

interface IEnhancedTableToolbar {
  date: Date
  onDateUpdate: (newDate: Date) => void;
  mode: ExtractionType
}

const EnhancedTableToolbar = (props: IEnhancedTableToolbar) => {
  const classes = useToolbarStyles();
  const [date, setDate] = React.useState(props.date)
  const { mode } = props
  const incrementType: any = {
    [ExtractionType.Daily]: 'days',
    [ExtractionType.Monthly]: 'months',
    [ExtractionType.Yearly]: 'years',
  }

  const changeDate = (opts?: { increment?: number, newDate?: Date }) => {
    if (opts?.increment) {
      const date = moment(props.date).add(opts.increment, incrementType[mode]).toDate()
      setDate(date)
      props.onDateUpdate(date)
    }
    if (opts?.newDate) {
      props.onDateUpdate(opts.newDate)
      setDate(opts.newDate)
    }
  }

  const handleDateChange = (e: any) => {
    changeDate({ newDate: moment(e).toDate() })
  }

  return (
    <Fragment>
      <div>
        <IconButton className={classes.arrowRight} onClick={() => changeDate({ increment: -1 })}>
          <ArrowBackIcon />
        </IconButton>
        <IconButton className={classes.arrowLeft} onClick={() => changeDate({ increment: 1 })}>
          <ArrowForwardIcon />
        </IconButton>
      </div>
      <div className={classes.title}>
        {mode === ExtractionType.Daily && (
          <DatePicker
            variant="inline"
            format="ddd DD MMMM YYYY"
            value={date}
            onChange={handleDateChange}
          />
        )}
        {mode === ExtractionType.Monthly && (
          <DatePicker
            views={["year", "month"]}
            variant="inline"
            value={date}
            onChange={handleDateChange}
          />
        )}
        {mode === ExtractionType.Yearly && (
          <DatePicker
            views={["year"]}
            variant="inline"
            value={date}
            onChange={handleDateChange}
          />
        )}
      </div>
    </Fragment>
  )
}

interface IExtractionTableProps {
  datas: EmployeeExtraction[] | undefined
  date: Date;
  mode: ExtractionType;
  onDateUpdate: (newDate: Date) => void
}

export default function ExtractionTable(props: IExtractionTableProps) {
  const classes = extractionTableStyle()
  const rowsPerPage = 10;
  const [page, setPage] = React.useState(0);
  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, (props.datas === undefined ? 0 : props.datas.length));

  const [rows, setRows] = React.useState<any>([])

  useEffect(() => {
    const computeRows = () => {
      if (props.datas) {
        const rowsFormated = props.datas.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row) => {
            return (
              <Row key={row.employeeId} row={row} />
            )
          })
        setRows(rowsFormated)
      }
    }
    computeRows()
  }, [props.datas, page])



  useEffect(() => {
    setPage(0)
  }, [props.date, props.datas])

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  return (
    <div style={{ position: 'relative' }}>
      <Paper>
        {props.datas === undefined && <CircularProgress className={classes.circularProgress} />}
        <EnhancedTableToolbar date={props.date} onDateUpdate={props.onDateUpdate} mode={props.mode} />
        <TableContainer component={Paper} className={`${props.datas === undefined ? classes.loadingTable : ''}`}>
          <Table aria-label="collapsible table">
            <TableHead>
              <TableRow>
                <TableCell />
                <TableCell>Employé</TableCell>
                <TableCell align="right">Jours travaillé</TableCell>
                <TableCell align="right">Heures travaillé</TableCell>
                <TableCell align="right">Heures d'absence</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows}
              {emptyRows > 0 && (
                <TableRow style={{ height: 63 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to} sur ${count !== -1 ? count : ""}`
          }
          count={props.datas?.length ?? 0}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={[10]}
          page={page}
          onChangePage={handleChangePage}
        />
      </Paper>
    </div>
  );
}

const extractionTableStyle = makeStyles({
  loadingTable: {
    opacity: 0.6
  },
  circularProgress: {
    position: 'absolute',
    zIndex: 5,
    margin: 'auto',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
});
