import { createStyles, Theme, WithStyles } from "@material-ui/core";
import { ExtractionType } from "../../types/ExtractionTypes";
import EmployeeExtraction from "../../types/EmployeeExtraction";

export interface IExtractionProps extends WithStyles<typeof ExtractionStyle> {
  onTabChange: (name: string) => void;

  shouldRefresh: boolean
}

export interface IExtractionState {
    mode: ExtractionType;
    employeeQuery: string;
    employeeExtractions?: EmployeeExtraction[];
    employeeExtractionsFiltered?: EmployeeExtraction[];
    date: Date;
}

export const ExtractionStyle = (theme: Theme) =>
  createStyles({
    title: {
      position: "relative",
      textAlign: "center",
      marginTop: 0,
      paddingTop: "20px",
    },
    settings: {
      width: "30%",
      marginRight: "auto",
      marginLeft: "auto",
    },
    records: {
      width: "60%",
      marginRight: "auto",
      marginLeft: "auto",
    },

    settingsPaper: {
      flexDirection: "column",
      display: "flex",
      textAlign: "center",
    },

    formContentRow: {
      width: "300px",
      marginBottom: "20px",
      marginLeft: "auto",
      marginRight: "auto",
    },
    backButton: {
      width: "300px",
      marginTop: "20px",
    }
  });
