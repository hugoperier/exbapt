import React from "react";
import { withStyles } from "@material-ui/core/styles";
import {
  IEmployeeTableProps,
  IEmployeeTableState,
  EmployeeTableStyle,
} from "./types";
import EnhancedTable from "./DisplayTable";

class EmployeeTable extends React.Component<
  IEmployeeTableProps,
  IEmployeeTableState
> {
  constructor(props: IEmployeeTableProps) {
    super(props);
    this.state = {
    };
  }    

  render() {
    const {employeeList, updateEmployee} = this.props

    return (
      <div>
        {employeeList && (
            <div>                
                <EnhancedTable data={employeeList} onEmployeeUpdateClick={updateEmployee} />
            </div>
        )}
      </div>
    );
  }
}

export default withStyles(EmployeeTableStyle)(EmployeeTable);
