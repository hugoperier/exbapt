import { createStyles, Theme, WithStyles } from "@material-ui/core";
import Employee from "../../../types/Employee";

export interface IEmployeeTableProps 
    extends WithStyles<typeof EmployeeTableStyle> {
        employeeList: Employee[] | null
        updateEmployee: (id: string) => void
    }

    export interface IEmployeeTableState {        
    }

export const EmployeeTableStyle = (theme: Theme) => 
    createStyles({

    })
