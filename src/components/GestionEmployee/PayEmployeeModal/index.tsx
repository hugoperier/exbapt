import {
  AppBar,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tabs,
  TextField,
  withStyles,
} from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';
import { TabContext, TabPanel } from "@material-ui/lab";
import moment from "moment";
import React, { Fragment } from "react";
import EmployeeExtraction from "../../../types/EmployeeExtraction";
import RTTPaid from "../../../types/RTTPaid";
import {
  IPayEmployeeModalProps,
  IPayEmployeeModalState,
  PayEmployeeModalStyle,
} from "./types";

class PayEmployeeModal extends React.PureComponent<
  IPayEmployeeModalProps,
  IPayEmployeeModalState
  > {
  constructor(props: IPayEmployeeModalProps) {
    super(props);
    this.state = {
      rtt: 0,
      tabValue: "0",
    };
  }

  componentDidUpdate = (oldProps: IPayEmployeeModalProps) => {
    const { open } = this.props

    if (!oldProps.open && open) {
      this.setState({ rtt: 0, tabValue: "0" })
    }
  }

  handlePay = () => {
    const { rtt } = this.state
    const { handlePay } = this.props

    handlePay(rtt)
    this.setState({ rtt: 0 })
  }

  historyTable = () => {
    const { rttPaids, employee, classes, handleDeletePay } = this.props

    const rttPaidsFiltered = rttPaids.filter(e => e.employeeId === employee.id).sort((a, b) => b.date.getTime() - a.date.getTime())

    return (
      <Fragment>
        <TableContainer className={classes.historyTable}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Date</TableCell>
                <TableCell align="right">Heures payés</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rttPaidsFiltered.map((row: RTTPaid) => (
                <Fragment>
                  <TableRow key={row.id}>
                    <TableCell component="th" scope="row">
                      {moment(row.date).format("DD/MM/YYYY")}
                    </TableCell>
                    <TableCell align="right">{row.rttPaid}</TableCell>
                    <IconButton onClick={() => handleDeletePay(row.id)} title="Supprimer cette entrée" color="secondary">
                      <DeleteIcon />
                    </IconButton>
                  </TableRow>
                </Fragment>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Fragment>
    );
  };

  payEmployee = () => {
    const { employeeEntries, employee, rttPaids } = this.props;
    const { rtt } = this.state;

    const currentRTT = EmployeeExtraction.getCurrentRtt(
      employeeEntries,
      employee,
      rttPaids,
      new Date()
    );

    return (
      <Fragment>
        <p>RTT actuel: {currentRTT}</p>
        <TextField
          fullWidth
          label="RTT payés"
          variant="outlined"
          value={rtt}
          onChange={(e: any) =>
            +e.target.value >= 0 && this.setState({ rtt: +e.target.value })
          }
          type="number"
          helperText={
            (currentRTT - rtt < 0) && rtt > 0 ? "⚠️ L'employé aura un RTT négatif" : ""
          }
        />
      </Fragment>
    );
  };

  render() {
    const {
      open,
      classes,
      handleClose,
      employee,
    } = this.props;
    const { rtt, tabValue } = this.state;

    return (
      <div>
        <Dialog
          open={open}
          onClose={handleClose}
          fullWidth
          className={classes.modal}
        >
          <TabContext value={tabValue}>
            <DialogTitle>Paiement des RTT</DialogTitle>
            <DialogContent>
              <AppBar position="static" color="default">
                <Tabs
                  value={+tabValue}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="fullWidth"
                  onChange={(e: any, value: number) =>
                    this.setState({ tabValue: value.toString() })
                  }
                >
                  <Tab label="Payer" />
                  <Tab label="Historique" />
                </Tabs>
              </AppBar>
              <h4>{`${employee.firstName} ${employee.lastName}`}</h4>
              <TabPanel value={"0"}>
                <this.payEmployee />
              </TabPanel>
              <TabPanel value={"1"}>
                <this.historyTable />
              </TabPanel>
            </DialogContent>
            <DialogActions>
              <Button
                variant="contained"
                onClick={handleClose}
                color="secondary"
              >
                Annuler
              </Button>
              <Button
                disabled={rtt === 0 || tabValue !== "0"}
                variant="contained"
                onClick={() => this.handlePay()}
                color="primary"
              >
                Valider
              </Button>
            </DialogActions>
          </TabContext>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(PayEmployeeModalStyle)(PayEmployeeModal);
