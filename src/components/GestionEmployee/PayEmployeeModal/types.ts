import { createStyles, WithStyles } from "@material-ui/core";
import Employee from "../../../types/Employee";
import EmployeeEntry from "../../../types/EmployeeEntry";
import RTTPaid from "../../../types/RTTPaid";

export interface IPayEmployeeModalProps
  extends WithStyles<typeof PayEmployeeModalStyle> {
  open: boolean;
  handleClose: () => void;
  handlePay: (rtt: number) => void;
  handleDeletePay: (id: string) => void;
  employee: Employee;
  employeeEntries: EmployeeEntry[];
  rttPaids: RTTPaid[];
}

export interface IPayEmployeeModalState {
  rtt: number;
  tabValue: string;
}

export const PayEmployeeModalStyle = () =>
  createStyles({
    modal: {
      textAlign: "center",
    },
    historyTable: {
      maxHeight: "400px"
    }
  });
