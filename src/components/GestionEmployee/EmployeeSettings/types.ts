import { createStyles, WithStyles } from "@material-ui/core";
import Employee from "../../../types/Employee";

export interface IGestionEmployeeProps
  extends WithStyles<typeof EmployeeSettingsStyle> {
  employees: Employee[];
  addEmployee: (e: Employee) => void;
  updateEmployee: (newEmployee: Employee) => void;
  cancelUpdate: () => void;
  employeeUpdateId: string;
  currentRTT: number
}

export interface IGestionEmployeeState {
  employee: Employee;
  errors: any;
  expanded: boolean
}

export const EmployeeSettingsStyle = () =>
  createStyles({
    employeeSettingPaper: {
      flexDirection: "column",
      display: "flex",
      textAlign: "center",
    },
    employeeSettingsInputForm: {
      width: "70%",
      paddingBottom: "20px",
      marginLeft: "auto",
      marginRight: "auto",
    },
    textExpend: {
      marginBottom: "10px",
      cursor: "pointer"
    },
  });
