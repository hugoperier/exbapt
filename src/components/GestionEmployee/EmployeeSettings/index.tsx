import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Paper, TextField, Button, FormControlLabel, Checkbox, Collapse } from "@material-ui/core";
import {
  IGestionEmployeeState,
  IGestionEmployeeProps,
  EmployeeSettingsStyle,
} from "./types";
import Employee from "../../../types/Employee";

class EmployeeSettings extends React.PureComponent<
  IGestionEmployeeProps,
  IGestionEmployeeState
  > {
  constructor(props: IGestionEmployeeProps) {
    super(props);
    this.state = {
      employee: {
        id: "",
        firstName: "",
        lastName: "",
        address: "",
        phone: "",
        email: "",
        baseRtt: 0,
        isActive: true
      },
      errors: {
        firstName: {
          errored: false,
          message: "",
        },
        lastName: {
          errored: false,
          message: "",
        },
        globalForm: {
          errored: false,
          message: "",
        },

      },
      expanded: false,
    };
  }

  componentDidUpdate(oldProps: IGestionEmployeeProps) {
    const { employeeUpdateId, employees } = this.props

    const errors = {
      firstName: {
        errored: false,
        message: "",
      },
      lastName: {
        errored: false,
        message: "",
      },
      globalForm: {
        errored: false,
        message: "",
      },
    };

    if (employeeUpdateId !== oldProps.employeeUpdateId && employeeUpdateId !== "") {
      const employeeInUpdate = employees.find((e: Employee) => e.id === employeeUpdateId)
      if (employeeInUpdate) {
        const employee = Object.assign({}, employeeInUpdate)
        this.setState({ employee, errors })
      }
    }
    else if (this.props !== oldProps) {
      const employee = {
        id: "",
        firstName: "",
        lastName: "",
        address: "",
        phone: "",
        email: "",
        baseRtt: 0,
        isActive: true
      }
      this.setState({ employee, errors, expanded: false })
    }
  }

  handleValidationButton = () => {
    const { employee } = this.state;
    const { addEmployee, employees, employeeUpdateId, updateEmployee } = this.props;
    let errors = {
      firstName: {
        errored: false,
        message: "",
      },
      lastName: {
        errored: false,
        message: "",
      },
      globalForm: {
        errored: false,
        message: "",
      },
    };

    const noErrors = (errors: any) => {
      for (const [, value] of Object.entries(errors)) {
        const v = value as any
        if (v.errored)
          return false
      }
      return true
    }

    if (employee.firstName === "") {
      errors.firstName.errored = true;
      errors.firstName.message = "Ce champs est obligatoire";
    }
    if (employee.lastName === "") {
      errors.lastName.errored = true;
      errors.lastName.message = "Ce champs est obligatoire";
    }
    if (
      employeeUpdateId === "" &&
      employees.some(
        (e: Employee) =>
          e.firstName.toUpperCase() === employee.firstName.toUpperCase() &&
          e.lastName.toUpperCase() === employee.lastName.toUpperCase()
      )
    ) {
      errors.globalForm.errored = true;
      errors.globalForm.message = "Cette personne est déja enregistrée";
    }

    if (
      employeeUpdateId !== "" &&
      employees.some(
        (e: Employee) =>
          e.firstName.toUpperCase() === employee.firstName.toUpperCase() &&
          e.lastName.toUpperCase() === employee.lastName.toUpperCase() &&
          e.id !== employee.id
      )
    ) {
      errors.globalForm.errored = true;
      errors.globalForm.message = "Cette personne est déja enregistrée";
    }

    if (noErrors(errors) && employeeUpdateId !== "") {
      updateEmployee(employee)
    }
    else if (noErrors(errors) && employeeUpdateId === "") {
      addEmployee(employee)
    }
    this.setState({ errors })
  };

  onBaseRTTChange = (e: any) => {
    const { employee } = this.state

    if (e.target.value) {
      const n = Number(e.target.value);
      this.setState({
        employee: { ...employee, baseRtt: n },
      })
    }
  }

  render() {
    const { classes, employeeUpdateId, cancelUpdate, currentRTT } = this.props;
    const { employee, errors, expanded } = this.state;

    return (
      <div>
        <Paper elevation={3} className={classes.employeeSettingPaper}>
          <h2>{employeeUpdateId !== "" ? "Modifier" : "Ajouter"} un employé</h2>
          <TextField
            label="Prénom"
            variant="outlined"
            className={classes.employeeSettingsInputForm}
            value={employee.firstName}
            error={errors["firstName"].errored}
            onChange={(e) =>
              this.setState({
                employee: { ...employee, firstName: e.target.value },
              })
            }
            helperText={
              errors["firstName"].errored ? errors["firstName"].message : ""
            }
          />
          <TextField
            label="Nom"
            variant="outlined"
            value={employee.lastName}
            className={classes.employeeSettingsInputForm}
            error={errors["lastName"].errored}
            onChange={(e) =>
              this.setState({
                employee: { ...employee, lastName: e.target.value },
              })
            }
            helperText={
              errors["lastName"].errored ? errors["lastName"].message : ""
            }
          />
          <TextField
            label="Base RTT"
            type="number"
            value={employee.baseRtt}
            variant="outlined"
            className={classes.employeeSettingsInputForm}
            onChange={this.onBaseRTTChange}
          />
          <Collapse in={employeeUpdateId !== ""}>
            <FormControlLabel
              className={classes.employeeSettingsInputForm}
              control={
                <Checkbox
                  checked={employee.isActive}
                  onChange={() => this.setState({ employee: { ...employee, isActive: !employee.isActive } })}
                  name="checked"
                  color="primary"
                />
              }
              label="Actif"
            />
          </Collapse>
          <Collapse in={expanded}>
            <TextField
              label="Adresse"
              variant="outlined"
              value={employee.address}
              onChange={(e) =>
                this.setState({
                  employee: { ...employee, address: e.target.value },
                })
              }
              className={classes.employeeSettingsInputForm}
            />
            <TextField
              label="Numero de telephone"
              variant="outlined"
              value={employee.phone}
              onChange={(e) =>
                this.setState({
                  employee: { ...employee, phone: e.target.value },
                })
              }
              className={classes.employeeSettingsInputForm}
            />
            <TextField
              label="Adresse email"
              variant="outlined"
              value={employee.email}
              onChange={(e) =>
                this.setState({
                  employee: { ...employee, email: e.target.value },
                })
              }
              className={classes.employeeSettingsInputForm}
            />
            {employeeUpdateId !== "" && (
              <p>RTT Actuel: {currentRTT}</p>
            )}
          </Collapse>
          <div className={classes.textExpend} onClick={() => this.setState({ expanded: !expanded })}>{expanded ? "Afficher moins" : "Afficher plus"}</div>
          {errors["globalForm"].errored && (
            <div style={{ color: "red" }}>{errors["globalForm"].message}</div>
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleValidationButton}
          >
            {employeeUpdateId !== "" ? "Modifier" : "Ajouter"}
          </Button>
          {employeeUpdateId !== "" && (
            <Button
              variant="contained"
              color="secondary"
              onClick={() => cancelUpdate()}
            >
              Annuler
            </Button>
          )}
        </Paper>
      </div>
    );
  }
}

export default withStyles(EmployeeSettingsStyle)(EmployeeSettings);
