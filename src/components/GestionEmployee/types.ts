import { createStyles, Theme, WithStyles } from "@material-ui/core";
import Employee from "../../types/Employee";
import EmployeeEntry from "../../types/EmployeeEntry";
import RTTPaid from "../../types/RTTPaid";

export interface IGestionEmployeeProps
  extends WithStyles<typeof GestionEmployeeStyle> {
  onTabChange: (name: string) => void;

  shouldRefresh: boolean
}

export interface IGestionEmployeeState {
  employees: Employee[];
  employeeUpdateId: string;
  payEmployeeModalOpen: boolean;
  employeeEntries: EmployeeEntry[];
  rttPaids: RTTPaid[]
}

export const GestionEmployeeStyle = (theme: Theme) =>
  createStyles({
    title: {
      position: "relative",
      textAlign: "center",
      marginTop: 0,
      paddingTop: "20px",
    },
    gridAddEmployee: {
      textAlign: "center",
      width: "30%",
      marginRight: "auto",
      marginLeft: "auto",
    },
    returnButton: {
      marginTop: "40px",
      width: "100%",
    },
    gridShowEmployee: {
      width: "60%",
      marginRight: "auto",
      marginLeft: "auto",
    },
    payRTTButton: {
      width: "100%",
      color: "",
      marginTop: "20px",
    }
  });
