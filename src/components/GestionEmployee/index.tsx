import React from "react";
import { Grid, withStyles, Button, Collapse } from "@material-ui/core";
import {
  getAllEmployee,
  addEmployee,
  updateEmployee,
  getRecords,
  getRttPaids,
  addRttPaid,
  removeRttPaid,
} from "../../services/db";
import EmployeeTable from "./EmployeeTable/EmployeeTable";
import {
  IGestionEmployeeProps,
  IGestionEmployeeState,
  GestionEmployeeStyle,
} from "./types";
import Employee from "../../types/Employee";
import KeyboardReturnIcon from "@material-ui/icons/KeyboardReturn";
import EmployeeSettings from "./EmployeeSettings";
import PayEmployeeModal from "./PayEmployeeModal";
import EmployeeExtraction from "../../types/EmployeeExtraction";
import RTTPaid from "../../types/RTTPaid";

class GestionEmployee extends React.PureComponent<
  IGestionEmployeeProps,
  IGestionEmployeeState
  > {
  constructor(props: IGestionEmployeeProps) {
    super(props);
    this.state = {
      employees: [],
      employeeUpdateId: "",
      payEmployeeModalOpen: false,
      employeeEntries: [],
      rttPaids: []
    };
  }

  componentDidMount = async () => {
    const employees = await getAllEmployee();
    const employeeEntries = await getRecords()
    const rttPaids = await getRttPaids()

    this.setState({ employees, employeeEntries, rttPaids });
  };

  updateEmployee = async (id: string = "") => {
    this.setState({ employeeUpdateId: id });
  };

  editEmployee = async (newEmployee: Employee) => {
    const { employees } = this.state;

    if (employees) {
      const index = employees.findIndex(
        (e: Employee) => e.id === newEmployee.id
      )
      if (index !== -1) {
        this.setState({
          employees: [
            ...employees.slice(0, index),
            Object.assign({}, employees[index], newEmployee),
            ...employees.slice(index + 1)
          ],
          employeeUpdateId: ""
        });
        await updateEmployee(newEmployee)
      }
    }
  }

  addEmployees = async (employee: Employee) => {
    const { employees } = this.state;

    if (employees) {
      const res: Employee = await addEmployee(employee);
      const newEmployeeList = [...employees, res];
      this.setState({ employees: newEmployeeList });
    } else {
      const res: Employee = await addEmployee(employee);
      const employeeList = [res];
      this.setState({ employees: employeeList });
    }
  };

  handlePay = async (rtt: number) => {
    const {employeeUpdateId, rttPaids} = this.state

    if (employeeUpdateId !== "") {
      const newRTTPayRecord: RTTPaid = {
        employeeId: employeeUpdateId,
        id: "",
        rttPaid: rtt,
        date: new Date()
      }
      const res = await addRttPaid(newRTTPayRecord)
      const rttPaidsUpdated = [...rttPaids, res]
      this.setState({rttPaids: rttPaidsUpdated})
    }
  }

  handleDeletePay = async (payId: string) => {
    const {rttPaids} = this.state

    const rttPaidsFiltered = rttPaids.filter(e => e.id !== payId)
    removeRttPaid(payId)
    this.setState({rttPaids: rttPaidsFiltered})
  }

  render() {
    const { classes } = this.props;
    const { employees, employeeUpdateId, payEmployeeModalOpen, employeeEntries, rttPaids } = this.state;

    const currentEmployee = employees.find(e => e.id === employeeUpdateId)
    const currentRTT = currentEmployee ? EmployeeExtraction.getCurrentRtt(employeeEntries, currentEmployee, rttPaids, new Date()) : 0

    return (
      <div>
        <div className={classes.title}>
          <h1>Gestion des employés</h1>
        </div>
        {currentEmployee && (
          <PayEmployeeModal handleDeletePay={this.handleDeletePay} employeeEntries={employeeEntries} rttPaids={rttPaids} employee={currentEmployee} open={payEmployeeModalOpen} handleClose={() => this.setState({ payEmployeeModalOpen: false })} handlePay={this.handlePay} />
        )}
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="flex-start"
        >
          <Grid item className={classes.gridAddEmployee}>
            <EmployeeSettings employees={employees} addEmployee={this.addEmployees} updateEmployee={this.editEmployee} employeeUpdateId={employeeUpdateId} cancelUpdate={this.updateEmployee} currentRTT={currentRTT} />
            <Collapse in={employeeUpdateId !== ""}>
              <Button
                variant="contained"
                className={classes.payRTTButton}
                color="primary"
                onClick={() => this.setState({ payEmployeeModalOpen: true })}
              >
                Payer des RTT
          </Button>
            </Collapse>
            <Button
              variant="contained"
              className={classes.returnButton}
              color="secondary"
              onClick={() => this.props.onTabChange("home")}
              startIcon={<KeyboardReturnIcon />}
            >
              Retour
          </Button>
          </Grid>
          <Grid item className={classes.gridShowEmployee}>
            <EmployeeTable
              employeeList={employees}
              updateEmployee={this.updateEmployee}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(GestionEmployeeStyle)(GestionEmployee);
