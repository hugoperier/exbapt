import { createStyles, WithStyles } from "@material-ui/core";
import Employee from "../../types/Employee";
import EmployeeEntry from "../../types/EmployeeEntry";
import { ImportOperation } from "../../types/ImportOperations";

export interface IDragDropFilesProps
  extends WithStyles<typeof DragDropFilesStyle> {
  files: File[];

  onSubmit: () => void;
}

export interface IDragDropFilesState {
  employees: Employee[];
  employeeRecords: EmployeeEntry[];
  date: Date;

  operationsDate: any;
  operations: ImportOperation[];

  operationInUpdate: ImportOperation | undefined
}

export const DragDropFilesStyle = () =>
  createStyles({
    fileTitle: {
      fontWeight: "bold",
      textAlign: "center",
    },
    datePicker: {
      marginBottom: "20px"
    },
    operationErrored: {
      background: "red !important",
      opacity: "0.8 !important",
      borderRadius: "25px",
      color: "white",
      padding: "5px",
      marginBottom: "5px",
    },
    operation: {
      borderRadius: "25px",
      color: "white",
      padding: "5px",
      marginBottom: "5px",
    },
    operationTitle: {
      display: "inline",
    },
    iconActions: {
      display: "inline",
      float: "right",
      paddingRight: "10px"
    },
    employeeInfo: {
      display: "inline",
      paddingLeft: "20px"
    },
  });
