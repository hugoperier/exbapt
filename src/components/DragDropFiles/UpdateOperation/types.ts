import { createStyles, WithStyles } from "@material-ui/core";
import { ImportOperation } from "../../../types/ImportOperations";

export interface IUpdateOperationProps
  extends WithStyles<typeof UpdateOperationStyle> {
  operation: ImportOperation | undefined;

  onClose: () => void;
  onSubmit: (operation: ImportOperation) => void;
}

export interface IUpdateOperationState {
  operationInUpdate: any; // EmployeeOperation | EmployeeEntryOperation | undefined
}

export const UpdateOperationStyle = () =>
  createStyles({
    employeeSettingsInputForm: {
      width: "70%",
      paddingBottom: "20px",
      marginLeft: "auto",
      marginRight: "auto",
    },
    employeeSettings: {
      flexDirection: "column",
      display: "flex",
      textAlign: "center",
      width: "500px",
    },
    dialogButtons: {
      marginLeft: "auto",
      marginRight: "auto",
    },
  });
