import React, { Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { IUpdateOperationProps, IUpdateOperationState, UpdateOperationStyle } from "./types";
import { Button, Checkbox, Dialog, DialogActions, DialogContent, DialogTitle, FormControlLabel, FormGroup, TextField } from "@material-ui/core";
import { EmployeeEntryOperation, EmployeeOperation, ImportOperationType } from "../../../types/ImportOperations";

class UpdateOperation extends React.PureComponent<IUpdateOperationProps, IUpdateOperationState> {
    constructor(props: IUpdateOperationProps) {
        super(props)

        this.state = {
            operationInUpdate: undefined
        }
    }

    componentDidUpdate(oldProps: IUpdateOperationProps) {
        if (oldProps.operation !== this.props.operation) {
            this.setState({ operationInUpdate: this.props.operation });
        }
    }

    closeUpdate = (e: any) => {
        this.setState({ operationInUpdate: undefined })
        this.props.onClose()
        e.stopPropagation()
    }

    submit = (e: any) => {
        const { operationInUpdate } = this.state

        this.props.onSubmit(operationInUpdate)
        e.stopPropagation()
    }

    onRestaurantCheckBoxChange = () => {
        const { operationInUpdate } = this.state

        this.setState({
            operationInUpdate: {
                ...operationInUpdate,
                entry: {
                    ...operationInUpdate.entry,
                    restaurant: !operationInUpdate.entry.restaurant,
                    worksiteMeal: false,
                },
            },
        });
    };

    onWsmCheckBoxChange = () => {
        this.setState((prevstate) => ({
            operationInUpdate: {
                ...prevstate.operationInUpdate,
                entry: {
                    ...prevstate.operationInUpdate.entry,
                    worksiteMeal: !prevstate.operationInUpdate.entry.worksiteMeal,
                    restaurant: false,
                },
            },
        }));
    };


    render() {
        const { operationInUpdate } = this.state
        const { classes } = this.props

        const isCreateEmployee = operationInUpdate && operationInUpdate.type === ImportOperationType.CreateEmployee
        const isEmployeeEntry = operationInUpdate && operationInUpdate.type !== ImportOperationType.CreateEmployee

        return (
            <div>
                <Dialog
                    open={operationInUpdate !== undefined}
                    onClose={this.closeUpdate}
                >
                    {isCreateEmployee && (
                        this.createEmployeeForm()
                    )}
                    {isEmployeeEntry && (
                        this.employeeEntryForm()
                    )}
                    {((isEmployeeEntry || isCreateEmployee) &&
                        <DialogActions className={classes.dialogButtons}>
                            <Button
                                variant="contained"
                                onClick={this.closeUpdate}
                                color="secondary"
                            >
                                Annuler
                            </Button>
                            <Button
                                variant="contained"
                                onClick={this.submit}
                                color="primary"
                            >
                                Valider
                            </Button>
                        </DialogActions>
                    )}
                </Dialog>
            </div>
        )
    }

    createEmployeeForm() {
        const { operationInUpdate } = this.state
        const { classes } = this.props

        const operation = operationInUpdate as EmployeeOperation;
        return (
            <Fragment>
                <DialogTitle>
                    Modifier la création d'employé
                </DialogTitle>
                <DialogContent className={classes.employeeSettings}>
                    <TextField
                        label="Prénom"
                        variant="outlined"
                        className={classes.employeeSettingsInputForm}
                        value={operation.newEmployee.firstName}
                        onChange={(e) => this.setState({ operationInUpdate: { ...operationInUpdate, employeeInfo: `${e.target.value} ${operationInUpdate.newEmployee.lastName}` ,newEmployee: { ...operationInUpdate.newEmployee, firstName: e.target.value } } })}
                        required
                    />
                    <TextField
                        label="Nom"
                        variant="outlined"
                        value={operation.newEmployee.lastName}
                        className={classes.employeeSettingsInputForm}
                        onChange={(e) => this.setState({ operationInUpdate: { ...operationInUpdate, employeeInfo: `${operationInUpdate.newEmployee.firstName} ${e.target.value}` , newEmployee: { ...operationInUpdate.newEmployee, lastName: e.target.value } } })}
                        required
                    />
                    <TextField
                        label="Base RTT"
                        type="number"
                        variant="outlined"
                        value={operationInUpdate.newEmployee.baseRtt}
                        className={classes.employeeSettingsInputForm}
                        onChange={(e) => this.setState({ operationInUpdate: { ...operationInUpdate, newEmployee: { ...operationInUpdate.newEmployee, baseRtt: +e.target.value } } })}
                    />
                    <TextField
                        label="Adresse"
                        variant="outlined"
                        value={operationInUpdate.newEmployee.address}
                        className={classes.employeeSettingsInputForm}
                        onChange={(e) => this.setState({ operationInUpdate: { ...operationInUpdate, newEmployee: { ...operationInUpdate.newEmployee, address: e.target.value } } })}
                    />
                    <TextField
                        label="Numéro de téléphone"
                        variant="outlined"
                        value={operationInUpdate.newEmployee.phone}
                        className={classes.employeeSettingsInputForm}
                        onChange={(e) => this.setState({ operationInUpdate: { ...operationInUpdate, newEmployee: { ...operationInUpdate.newEmployee, phone: e.target.value } } })}
                    />
                    <TextField
                        label="Email"
                        variant="outlined"
                        value={operationInUpdate.newEmployee.email}
                        className={classes.employeeSettingsInputForm}
                        onChange={(e) => this.setState({ operationInUpdate: { ...operationInUpdate, newEmployee: { ...operationInUpdate.newEmployee, email: e.target.value } } })}
                    />
                </DialogContent>
            </Fragment>
        )
    }

    employeeEntryForm() {
        const { operationInUpdate } = this.state
        const { classes } = this.props

        const operation = operationInUpdate as EmployeeEntryOperation;
        return (
            <Fragment>
                <DialogTitle>
                    {operation.type === ImportOperationType.CreateEmployeeEntrie ? "Création d'entrée employée" : "Modification d'entrée employée"}
                </DialogTitle>
                <DialogContent className={classes.employeeSettings}>
                    <TextField
                        label="Employé"
                        value={operation.employeeInfo}
                        disabled
                        className={classes.employeeSettingsInputForm}
                        variant="outlined"
                    />
                    <TextField
                        label="Heures travaillés"
                        className={classes.employeeSettingsInputForm}
                        value={operationInUpdate.entry.hoursWorked}
                        variant="outlined"
                        type="number"
                        onChange={(e) => this.setState({ operationInUpdate: { ...operationInUpdate, entry: { ...operationInUpdate.entry, hoursWorked: +e.target.value } } })}
                    />
                    <FormGroup row className={classes.dialogButtons}>
                        {[3, 4, 5, 6].map((value: number) => {
                            return (
                                <FormControlLabel
                                    key={`${value}mg`}
                                    labelPlacement="top"
                                    label={`${value} mg`}
                                    control={
                                        <Checkbox
                                            checked={operationInUpdate.entry.mg === value}
                                            onChange={() => this.setState((prevstate) => ({
                                                operationInUpdate: {
                                                    ...prevstate.operationInUpdate,
                                                    entry: {
                                                        ...operationInUpdate.entry,
                                                        mg: value === operationInUpdate.entry.mg ? 0 : value,
                                                    },
                                                },
                                            }))
                                            }
                                            name={`${value}mg`}
                                        />
                                    }
                                />

                            );
                        })}
                    </FormGroup>
                    <FormControlLabel
                        className={classes.dialogButtons}
                        control={
                            <Checkbox
                                checked={operationInUpdate.entry.worksiteMeal}
                                onChange={this.onWsmCheckBoxChange}
                                name="worksitemeal"
                            />
                        }
                        label="Repas Chantier"
                    />
                    <FormControlLabel
                        className={classes.dialogButtons}
                        control={
                            <Checkbox
                                checked={operationInUpdate.entry.restaurant}
                                name="restaurant"
                                onChange={this.onRestaurantCheckBoxChange}
                            />
                        }
                        label="Repas Restaurant"
                    />
                </DialogContent>
            </Fragment>
        )
    }
}

export default withStyles(UpdateOperationStyle)(UpdateOperation)