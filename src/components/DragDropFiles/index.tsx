import React, { Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { DragDropFilesStyle, IDragDropFilesProps, IDragDropFilesState } from "./types";
import { getAllEmployee, getRecords } from "../../services/db";
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, TextField } from "@material-ui/core";
import EmployeeImporter from "../../types/EmployeeImport";
import { EmployeeEntryOperation, EmployeeOperation, ImportOperation, ImportOperationType } from "../../types/ImportOperations";
import MoreVertIcon from '@material-ui/icons/MoreVert';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import RestoreIcon from '@material-ui/icons/Restore';
import moment from "moment";
import UpdateOperation from "./UpdateOperation";

class DragDropFiles extends React.PureComponent<IDragDropFilesProps, IDragDropFilesState> {
    constructor(props: IDragDropFilesProps) {
        super(props)

        this.state = {
            employees: [],
            employeeRecords: [],
            date: new Date(),
            operationsDate: {},
            operations: [],
            operationInUpdate: undefined
        }
    }

    componentDidUpdate = async (oldProps: IDragDropFilesProps) => {
        const { files } = this.props
        const { employees, employeeRecords } = this.state

        if (oldProps.files !== files) {
            const importer = new EmployeeImporter(files, employees, employeeRecords);
            const operations = await importer.compute()
            const operationsParsed = operations.sort((a, b) => {
                if (a.fileName !== b.fileName)
                    return 1
                if (a.type === ImportOperationType.CreateEmployee && b.type !== ImportOperationType.CreateEmployee)
                    return -1
                return 0
            })
            this.setState({ operations: operationsParsed })
        }
    }



    componentDidMount = async () => {
        const employees = await getAllEmployee();
        const employeeRecords = await getRecords();

        this.setState({ employees, employeeRecords });
    }

    importOperation = async () => {
        const { operations, operationsDate } = this.state

        const notValid = operations.filter(e => !e.isActive || this.isErrored(e))
        const valid = operations.filter(e => e.isActive && !this.isErrored(e))
        const notPassedOperations = [...notValid, ...(await EmployeeImporter.import(valid, operationsDate))]
        this.setState({ operations: notPassedOperations }, () => {
            this.props.onSubmit()
        })
        const employees = await getAllEmployee();
        const employeeRecords = await getRecords();
        this.setState({ employees, employeeRecords })
    }

    DisplayOperationInfos(operation: ImportOperation) {
        if (!operation.isActive)
            return "Opération non prise en compte"
        if (operation.type === ImportOperationType.CreateEmployee) {
            const employee = operation as EmployeeOperation
            return `
            Création d'employée:\nNom: ${employee.newEmployee.lastName}\nPrénom: ${employee.newEmployee.firstName}`
        }
        else // EmployeeEntry
        {
            const entry = operation as EmployeeEntryOperation
            return `Modification d'entrée de donnée:\nNom: ${entry.entry.employee?.lastName}\nPrénom: ${entry.entry.employee?.firstName}\nHeures travaillés: ${entry.entry.hoursWorked}\nMg: ${entry.entry.mg}`
        }
    }

    handleDateFileChange(e: any, fileName: string) {
        const { operations, operationsDate, employeeRecords } = this.state

        const operationAffected = operations.filter((e) => e.fileName === fileName)
        const operationsUpdated = [...operations.filter((e) => e.fileName !== fileName), ...EmployeeImporter.updateByDate(operationAffected, new Date(e.target.value), employeeRecords)]

        const newOperationDate = { ...operationsDate, [fileName]: e.target.value }
        this.setState({ operationsDate: newOperationDate, operations: operationsUpdated })
    }

    isErrored(operation: ImportOperation) {
        const { operations, employees } = this.state

        if (!operation.isActive)
            return undefined
        if (operation.type !== ImportOperationType.CreateEmployee) {
            const entry = operation as EmployeeEntryOperation
            const employeeExist = employees.some(e => e.firstName.toLowerCase() === entry.entry.employee?.firstName.toLowerCase() && e.lastName.toLowerCase() === entry.entry.employee?.lastName.toLowerCase())
            const hasBeenCreated = operations.some(e => e.type === ImportOperationType.CreateEmployee && e.employeeInfo === operation.employeeInfo && e.isActive)
            return !(employeeExist || hasBeenCreated) ? "Erreur: Aucun employé associé a cette entrée" : undefined
        }
        const creation = operation as EmployeeOperation
        const employeeAlreadyExist = employees.some(e => e.firstName.toLowerCase() === creation.newEmployee.firstName.toLowerCase() && e.lastName.toLowerCase() === creation.newEmployee.lastName.toLowerCase())
        return employeeAlreadyExist ? "Erreur: Cet employé existe déja" : undefined
    }

    changeOperation(operation: ImportOperation, active: boolean) {
        const { operations } = this.state

        const index = operations.indexOf(operation)
        if (index !== -1) {
            const operationChanged = { ...operation, isActive: active }
            this.setState({
                operations: [
                    ...operations.slice(0, index),
                    Object.assign({}, operations[index], operationChanged),
                    ...operations.slice(index + 1)
                ]
            })
        }
    }

    onOperationUpdate = (cur: ImportOperation) => {
        const { operationInUpdate, operations } = this.state

        if (operationInUpdate) {
            const index = operations.findIndex(
                (op: ImportOperation) =>
                    op === operationInUpdate
            )
            if (index !== -1)
                this.setState({
                    operations: [
                        ...operations.slice(0, index),
                        Object.assign({}, operations[index], cur),
                        ...operations.slice(index + 1)
                    ],
                    operationInUpdate: undefined
                })
        }
    }

    render() {
        const { operations } = this.state

        const isOpen = operations.length > 0
        return (
            <Fragment>
                <Dialog
                    open={isOpen}
                    onClose={() => this.setState({ operations: [] })}
                    fullWidth
                >
                    <DialogTitle>Importation de données</DialogTitle>
                    <DialogContent>
                        {this.operationTable()}
                    </DialogContent>
                    <DialogActions>
                        <Button
                            variant="contained"
                            onClick={() => this.setState({ operations: [] })}
                            color="secondary"
                        >
                            Annuler
                    </Button>
                        <Button
                            variant="contained"
                            onClick={this.importOperation}
                            color="primary"
                        >
                            Valider
                    </Button>
                    </DialogActions>
                </Dialog>
            </Fragment>
        )
    }

    operationTable = () => {
        const { operations, operationsDate, operationInUpdate } = this.state
        const { classes } = this.props

        const operationsLabel = {
            [ImportOperationType.CreateEmployee]: {
                title: "Création d'employé",
                color: "#3f51b5"
            },
            [ImportOperationType.UpdateEmployeeEntrie]: {
                title: "Mise à jour d'une entrée",
                color: "#f50057"
            },
            [ImportOperationType.CreateEmployeeEntrie]: {
                title: "Ajout d'une entrée",
                color: "#075700"
            },
        }

        return (
            <div>
                <UpdateOperation onSubmit={this.onOperationUpdate} operation={operationInUpdate} onClose={() => this.setState({ operationInUpdate: undefined })} />
                {operations.map((e: ImportOperation, i, self) => {
                    return (
                        <Fragment key={i}>
                            {(i === 0 ||
                                e.fileName !== self[i - 1].fileName) && (
                                    <Fragment>
                                        <div className={classes.fileTitle} style={i > 0 ? { paddingTop: "20px" } : undefined}>{e.fileName}</div>
                                        <TextField
                                            fullWidth
                                            className={classes.datePicker}
                                            type="date"
                                            size="small"
                                            value={moment(operationsDate[e.fileName]).format('yyyy-MM-DD')}
                                            onChange={(event) => this.handleDateFileChange(event, e.fileName)}
                                            variant="outlined"
                                            InputLabelProps={{
                                                shrink: true,
                                            }} />
                                    </Fragment>
                                )
                            }
                            <div title={this.isErrored(e) || this.DisplayOperationInfos(e)} className={this.isErrored(e) ? classes.operationErrored : classes.operation} style={{ background: operationsLabel[e.type].color, opacity: e.isActive ? 1 : 0.4 }}>
                                <div className={classes.operationTitle}>{operationsLabel[e.type].title}</div>
                                <div className={classes.employeeInfo}>{e.employeeInfo}</div>
                                <div className={classes.iconActions}>
                                    <IconButton title="Afficher plus d'informations" onClick={() => this.setState({ operationInUpdate: e })} style={{ padding: "0px 10px 0px 0px" }}>
                                        <MoreVertIcon />
                                    </IconButton>
                                    {e.isActive ? (<IconButton title="Supprimer" onClick={() => this.changeOperation(e, false)} style={{ padding: 0 }}>
                                        <DeleteForeverIcon />
                                    </IconButton>) : (
                                            <IconButton title="Restaurer" onClick={() => this.changeOperation(e, true)} style={{ padding: 0 }}>
                                                <RestoreIcon />
                                            </IconButton>
                                        )}
                                </div>
                            </div>
                        </Fragment>
                    )
                })
                }
            </div >
        )
    }

}

export default withStyles(DragDropFilesStyle)(DragDropFiles)
