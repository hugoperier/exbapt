export default interface RTTPaid {
    id: string;
    employeeId: string;
    rttPaid: number;
    date: Date;
}