export default interface Employee {
    id: string;
    firstName: string;
    lastName: string;
    address: string;
    phone: string;
    email: string
    baseRtt: number;
    isActive: boolean;    
}
