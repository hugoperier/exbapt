export default interface Note {
  id: string;
  content: string;
  date: Date;
}
