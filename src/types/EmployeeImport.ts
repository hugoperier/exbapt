import moment from "moment";
import {
  addEmployee,
  addRecord,
  getAllEmployee,
  getRecords,
  updateEmployeeRecord,
} from "../services/db";
import Employee from "./Employee";
import EmployeeEntry from "./EmployeeEntry";
import { IImport } from "./IImport";
import {
  EmployeeEntryOperation,
  EmployeeOperation,
  ImportOperation,
  ImportOperationType,
} from "./ImportOperations";

export default class EmployeeImporter implements IImport {
  public files: File[];
  public employees: Employee[];
  public employeeEntries: EmployeeEntry[];

  employeesAdded: any; // {firstName, lastName}

  constructor(files: File[], employees: Employee[], entries: EmployeeEntry[]) {
    this.files = files;
    this.employees = employees;
    this.employeeEntries = entries;
    this.employeesAdded = [];
  }

  public static import = async (
    operations: ImportOperation[],
    operationsDate: any
  ): Promise<ImportOperation[]> => {
    let errored: ImportOperation[] = [];
    let employees: Employee[] = await getAllEmployee();
    let employeeEntries: EmployeeEntry[] = await getRecords();

    for (const operation of operations) {
      if (operation.type === ImportOperationType.CreateEmployee) {
        const createOperation = operation as EmployeeOperation;
        const res = await addEmployee(createOperation.newEmployee);
        employees.push(res);
      } else {
        const entryOperation = operation as EmployeeEntryOperation;
        const employee = employees.find(
          (e) =>
            e.firstName.toLowerCase() ===
              entryOperation.entry.employee?.firstName.toLowerCase() &&
            e.lastName.toLowerCase() ===
              entryOperation.entry.employee?.lastName.toLowerCase()
        );
        if (!employee) {
          errored.push(operation);
          continue;
        }
        const entry: EmployeeEntry = {
          ...entryOperation.entry,
          date: operationsDate[entryOperation.fileName]
            ? new Date(operationsDate[entryOperation.fileName])
            : new Date(),
          employeeId: employee.id,
          employee,
        };
        const isAlreadyPresent =
          entry.id !== "" ||
          employeeEntries.some(
            (e) =>
              moment(e.date).format("yyyy-MM-DD") ===
                moment(entry.date).format("yyyy-MM-DD") &&
              entry.employee?.firstName.toLowerCase() ===
                e.employee?.firstName.toLowerCase() &&
              entry.employee?.lastName.toLowerCase() ===
                e.employee?.lastName.toLowerCase()
          );

        if (operation.type === ImportOperationType.CreateEmployeeEntrie) {
          if (isAlreadyPresent) {
            errored.push(operation);
            continue;
          }
          const res = await addRecord(entry);
          employeeEntries.push(res);
        } else if (
          operation.type === ImportOperationType.UpdateEmployeeEntrie &&
          isAlreadyPresent
        ) {
          await updateEmployeeRecord(entry.id, entry);
        }
      }
    }
    return errored;
  };

  verify = (fileParsed: string[]) => {
    if (!fileParsed[0].includes("Salaries")) return false;
    for (const line of fileParsed) {
      const lineSplitteds = line
        .split('"')
        .filter((_: string, i: number) => i % 2 === 1);
      if (lineSplitteds.length !== 0 && lineSplitteds.length !== 8)
        return false;
    }
    return true;
  };

  public static updateByDate = (
    data: ImportOperation[],
    date: Date,
    entries: EmployeeEntry[]
  ): ImportOperation[] => {
    const entriesFiltered = entries.filter(
      (e) =>
        moment(e.date).format("yyyy-MM-DD") ===
        moment(date).format("yyyy-MM-DD")
    );
    let operationModified: ImportOperation[] = [];

    for (const operation of data) {
      if (operation.type !== ImportOperationType.CreateEmployee) {
        const entryOperation = operation as EmployeeEntryOperation;
        const entryExisting = entriesFiltered.find(
          (e) =>
            e.employee?.firstName.toLowerCase() ===
              entryOperation.entry.employee?.firstName.toLowerCase() &&
            e.employee?.lastName.toLowerCase() ===
              entryOperation.entry.employee?.lastName.toLowerCase()
        );

        if (entryExisting) {
          const entryUpdated: EmployeeEntryOperation = {
            ...entryOperation,
            entry: { ...entryOperation.entry, id: entryExisting.id },
            type: ImportOperationType.UpdateEmployeeEntrie,
          };
          operationModified.push(entryUpdated);
        } else {
          const entryCreated: EmployeeEntryOperation = {
            ...entryOperation,
            entry: { ...entryOperation.entry, id: "" },
            type: ImportOperationType.CreateEmployeeEntrie,
          };
          operationModified.push(entryCreated);
        }
      } else operationModified.push(operation);
    }
    return operationModified;
  };

  compute = async (): Promise<ImportOperation[]> => {
    const operationContainers: ImportOperation[] = [];

    for (const file of this.files) {
      const fileParsed = (await file.text()).split("\n");

      if (this.verify(fileParsed)) {
        for (const line of fileParsed) {
          if (line && fileParsed.indexOf(line) > 0) {
            const lineFormatted = line
              .split('"')
              .filter((_: string, i: number) => i % 2 === 1);
            const employeeOperation = this.getEmployeeOperation(
              lineFormatted[0],
              file.name
            );
            const employeeEntryOperation = this.getEmployeeEntryOperation(
              lineFormatted,
              file.name
            );
            if (employeeOperation) operationContainers.push(employeeOperation);
            operationContainers.push(employeeEntryOperation);
          }
        }
      }
    }
    return operationContainers;
  };

  getEmployeeEntryOperation = (
    fields: string[],
    fileName: string,
    date: Date = new Date()
  ): EmployeeEntryOperation => {
    const [lastName, firstName] = fields[0]
      .split(" ")
      .map((e) => e?.toLowerCase());
    const mg = [fields[2], fields[3], fields[4], fields[5]].indexOf("1,00");
    const mgFormatted = mg === -1 || mg === undefined ? 0 : mg + 3;

    const firstNameParsed = firstName
      ? firstName.charAt(0).toUpperCase() + firstName.slice(1)
      : "";
    const lastNameParsed = lastName
      ? lastName.charAt(0).toUpperCase() + lastName.slice(1)
      : "";

    const entry: EmployeeEntry = {
      id: "",
      employeeId: "",
      employee: {
        id: "",
        firstName: firstNameParsed === "" ? lastNameParsed : firstNameParsed,
        lastName:
          lastNameParsed !== "" && firstNameParsed === "" ? "" : lastNameParsed,
        address: "",
        phone: "",
        email: "",
        baseRtt: 0,
        isActive: true,
      },
      hoursWorked: parseFloat(fields[1].replace(",", ".")),
      mg: mgFormatted,
      worksiteMeal: parseFloat(fields[6].replace(",", ".")) > 0,
      restaurant: parseFloat(fields[7].replace(",", ".")) > 0,
      absenceType: "",
      absenceHours: 0,
      publicHoliday: false,
      date: date,
    };
    const entryAlreadyExisting = this.employeeEntries
      .filter(
        (e) =>
          moment(e.date).format("yyyy-MM-DD") ===
          moment(date).format("yyyy-MM-DD")
      )
      .find(
        (e) =>
          e.employee?.firstName.toLowerCase() ===
            entry.employee?.firstName.toLowerCase() &&
          e.employee?.lastName.toLowerCase() ===
            entry.employee?.lastName.toLowerCase()
      );

    if (entryAlreadyExisting) {
      const entryModified: EmployeeEntry = {
        ...entryAlreadyExisting,
        hoursWorked: parseFloat(fields[1].replace(",", ".")),
        mg: mgFormatted,
        worksiteMeal: parseFloat(fields[6].replace(",", ".")) > 0,
        restaurant: parseFloat(fields[7].replace(",", ".")) > 0,
        absenceType: "",
        absenceHours: 0,
        publicHoliday: false,
      };
      return {
        entry: entryModified,
        fileName,
        type: ImportOperationType.UpdateEmployeeEntrie,
        isActive: true,
        employeeInfo:
          entryModified.employee?.firstName +
          " " +
          entryModified.employee?.lastName,
      };
    }
    return {
      entry,
      fileName,
      type: ImportOperationType.CreateEmployeeEntrie,
      isActive: true,
      employeeInfo:
        entry.employee?.firstName +
        (entry.employee?.lastName === "" ? "" : " ") +
        entry.employee?.lastName,
    };
  };

  getEmployeeOperation = (
    name: string,
    fileName: string
  ): EmployeeOperation | undefined => {
    const [lastName, firstName] = name.split(" ").map((e) => e?.toLowerCase());

    // SI un employé a un nom (stagiaire) et existe déja
    if (
      name.split(" ").length === 1 &&
      this.employees.some(
        (e: Employee) => e.firstName.toLowerCase() === lastName
      )
    )
      return;
    const employeeExist =
      this.employees.some(
        (e: Employee) =>
          e.firstName.toLowerCase() === firstName &&
          e.lastName.toLowerCase() === lastName
      ) ||
      this.employeesAdded.some(
        (e: any) =>
          e.firstName.toLowerCase() === firstName &&
          e.lastName.toLowerCase() === lastName
      );
    if (!employeeExist) {
      const firstNameParsed = firstName
        ? firstName.charAt(0).toUpperCase() + firstName.slice(1)
        : "";
      const lastNameParsed = lastName
        ? lastName.charAt(0).toUpperCase() + lastName.slice(1)
        : "";
      this.employeesAdded.push({
        firstName: firstNameParsed,
        lastName: lastNameParsed,
      });
      return {
        fileName,
        newEmployee: {
          id: "",
          firstName: firstNameParsed === "" ? lastNameParsed : firstNameParsed,
          lastName:
            lastNameParsed !== "" && firstNameParsed === ""
              ? ""
              : lastNameParsed,
          address: "",
          phone: "",
          email: "",
          baseRtt: 0,
          isActive: true,
        },
        isActive: true,
        type: ImportOperationType.CreateEmployee,
        employeeInfo:
          firstNameParsed +
          (firstNameParsed === "" ? "" : " ") +
          lastNameParsed,
      };
    }
  };
}
