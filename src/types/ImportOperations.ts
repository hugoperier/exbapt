import Employee from "./Employee";
import EmployeeEntry from "./EmployeeEntry";

export enum ImportOperationType {
  CreateEmployee,
  CreateEmployeeEntrie,
  UpdateEmployeeEntrie,
}

export interface ImportOperation {
  fileName: string;
  type: ImportOperationType;

  isActive: boolean;
  employeeInfo: string;
}

export interface EmployeeOperation extends ImportOperation {
  newEmployee: Employee;
}

export interface EmployeeEntryOperation extends ImportOperation {
  entry: EmployeeEntry;
}
