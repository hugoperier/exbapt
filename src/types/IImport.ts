import { Interface } from "readline";
import Employee from "./Employee";

export abstract class IImport {

    public files: File[]
    public employees: Employee[]

    constructor(files: File[], employees: Employee[]) {
        this.files = files;
        this.employees = employees
    }

    abstract verify(fileParsed: string[]): boolean;

    abstract compute(): void;
}