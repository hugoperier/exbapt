import EmployeeEntry from "./EmployeeEntry";
import { ExtractionType } from "./ExtractionTypes";
import moment from "moment";
import Employee from "./Employee";
import RTTPaid from "./RTTPaid";

export default class EmployeeExtraction {
  public employeeId: string;
  public firstName: string;
  public lastName: string;
  public entries: EmployeeEntry[];

  private entriesFormatted: EmployeeEntry[];
  private _date: Date = new Date();
  public get date(): Date {
    return this._date;
  }
  public set date(v: Date) {
    this._date = v;
    this.actualize();
  }

  private _mode: ExtractionType = 0;
  public get mode(): ExtractionType {
    return this._mode;
  }
  public set mode(v: ExtractionType) {
    this._mode = v;
    this.actualize();
  }

  static getCurrentRtt(
    entries: EmployeeEntry[],
    employee: Employee,
    rttPaids: RTTPaid[],
    date: Date
  ): number {
    const dateFormatted = moment(date)
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
      .toDate();
    const previousEntriesOfEmployee = entries.filter(
      (e) =>      
        e.employee?.id === employee.id &&
        moment(e.date).isBefore(moment(dateFormatted))
    );
    const rtt = previousEntriesOfEmployee.reduce(
      (acc: number, cur: EmployeeEntry) =>
        acc + (cur.absenceType === "" ? cur.hoursWorked - 7 : 0),
      employee.baseRtt
    );
    const rttPaid = rttPaids.reduce(
      (acc: number, cur: RTTPaid) =>
        acc + (cur.employeeId === employee.id ? cur.rttPaid : 0),
      0
    );
    return rtt - rttPaid;
  }

  private actualize() {
    const format = {
      [ExtractionType.Daily]: "DD-MM-YYYY",
      [ExtractionType.Monthly]: "MM-YYYY",
      [ExtractionType.Yearly]: "YYYY",
    };
    this.entriesFormatted = this.entries.filter(
      (v: EmployeeEntry) =>
        moment(v.date).format(format[this._mode]) ===
        moment(this._date).format(format[this._mode])
    );
  }

  constructor(
    employeeEntries: EmployeeEntry[],
    date: Date = new Date(),
    mode: ExtractionType = ExtractionType.Daily
  ) {
    this.entriesFormatted = [];
    this.entries = employeeEntries;
    this.date = date;
    this.firstName = employeeEntries[0].employee?.firstName || "";
    this.lastName = employeeEntries[0].employee?.lastName || "";
    this.employeeId = employeeEntries[0].employeeId;
    this.mode = mode;
    this.date = date;
  }

  public getTotalHoursWorked(): number {
    return this.entriesFormatted.reduce(
      (acc: number, cur: EmployeeEntry) => acc + cur.hoursWorked,
      0
    );
  }

  public getTotalDaysWorked(): number {
    return this.entriesFormatted.reduce(
      (acc: number, cur: EmployeeEntry) => acc + (cur.hoursWorked > 0 ? 1 : 0),
      0
    );
  }

  public getNbMg(mg: number): number {
    return this.entriesFormatted.reduce(
      (acc: number, cur: EmployeeEntry) => acc + (cur.mg === mg ? 1 : 0),
      0
    );
  }

  public getRestaurants(): number {
    return this.entriesFormatted.reduce(
      (acc: number, cur: EmployeeEntry) => acc + (cur.restaurant ? 1 : 0),
      0
    );
  }

  public getWorksiteMeal(): number {
    return this.entriesFormatted.reduce(
      (acc: number, cur: EmployeeEntry) => acc + (cur.worksiteMeal ? 1 : 0),
      0
    );
  }

  public getAbsences(absenceType?: string): number {
    if (absenceType) {
      return this.entriesFormatted.reduce(
        (acc: number, cur: EmployeeEntry) =>
          acc + (cur.absenceType === absenceType ? cur.absenceHours : 0),
        0
      );
    } else
      return this.entriesFormatted.reduce(
        (acc: number, cur: EmployeeEntry) =>
          acc + (cur.absenceType !== "" ? 1 : 0),
        0
      );
  }

  private getIntervalDate(customDate?: Date) {
    const date = customDate || this.date;

    const endDateFilter = {
      [ExtractionType.Daily]: new Date(moment(date).endOf("day").toDate()),
      [ExtractionType.Monthly]: new Date(moment(date).endOf("month").toDate()),
      [ExtractionType.Yearly]: new Date(moment(date).endOf("year").toDate()),
    };
    const beginDateFilter = {
      [ExtractionType.Daily]: new Date(moment(date).startOf("day").toDate()),
      [ExtractionType.Monthly]: new Date(
        moment(date).startOf("month").toDate()
      ),
      [ExtractionType.Yearly]: new Date(moment(date).startOf("year").toDate()),
    };
    return [beginDateFilter[this.mode], endDateFilter[this.mode]];
  }

  public getRttByDate(begin?: boolean): number {
    const date = this.getIntervalDate()[begin ? 0 : 1];

    if (this.entries.length > 0 && this.entries[0].employee) {
      const employee = this.entries[0].employee;
      const entriesFormatted = this.entries.filter((e) =>
        moment(e.date).isBefore(moment(date))
      );
      const rtt = entriesFormatted.reduce(
        (acc: number, cur: EmployeeEntry) =>
          acc + (cur.absenceType === "" ? cur.hoursWorked - 7 : 0),
        employee.baseRtt
      );
      return rtt;
    }
    console.error("Return 0 for getRttByDate, this should not happen");
    return 0;
  }

  public getAddedRTT(): number {
    const intervals = this.getIntervalDate();

    const startDate = intervals[0];
    const endDate = intervals[1];
    if (this.entries.length > 0 && this.entries[0].employee) {
      const entriesFormatted = this.entries.filter(
        (e) =>
          moment(e.date).isBefore(moment(endDate)) &&
          moment(e.date).isAfter(moment(startDate))
      );
      const rtt = entriesFormatted.reduce(
        (acc: number, cur: EmployeeEntry) =>
          acc + (cur.hoursWorked > 7 ? cur.hoursWorked - 7 : 0),
        0
      );
      return rtt;
    }
    console.error("Return 0 for getAddedRTT, this should not happen");
    return 0;
  }

  public getTakenRTT(): number {
    const intervals = this.getIntervalDate();

    const startDate = intervals[0];
    const endDate = intervals[1];
    if (this.entries.length > 0 && this.entries[0].employee) {
      const entriesFormatted = this.entries.filter(
        (e) =>
          moment(e.date).isBefore(moment(endDate)) &&
          moment(e.date).isAfter(moment(startDate))
      );
      const rtt = entriesFormatted.reduce(
        (acc: number, cur: EmployeeEntry) =>
          acc +
          (cur.absenceType === "" && cur.hoursWorked < 7
            ? cur.hoursWorked - 7
            : 0),
        0
      );
      return Math.abs(rtt);
    }
    console.error("Return 0 for getAddedRTT, this should not happen");
    return 0;
  }
}
