export enum ExtractionType {
    Daily,
    Monthly,
    Yearly
}