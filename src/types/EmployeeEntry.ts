import Employee from "./Employee";

export default interface EmployeeEntry {
  id: string
  employeeId: string
  employee: Employee | null;
  hoursWorked: number;
  mg: number;
  worksiteMeal: boolean;
  restaurant: boolean;
  absenceType: string;
  absenceHours: number;
  publicHoliday: boolean;
  date: Date;
}
