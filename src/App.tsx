import MomentUtils from "@date-io/moment";
import MuiPickersUtilsProvider from "@material-ui/pickers/MuiPickersUtilsProvider";
import React, { Fragment } from "react";
import "./App.css";
import DailyEntry from "./components/DailyEntry";
import Extraction from "./components/Extraction"
import "moment/locale/fr";
import { useDropzone } from 'react-dropzone';
import GestionEmployee from "./components/GestionEmployee";
import DragDropFiles from "./components/DragDropFiles";

function App() {

  const [tab, setTab] = React.useState("home")
  const [refresh, setRefresh] = React.useState(false)

  const handleRefresh = () => {
    setRefresh(true)
    setTimeout(() => {
      setRefresh(false)
    }, 2000)
  }
  const handleTabChange = (name: string) => {
    setTab(name)
  }
  const { getRootProps, getInputProps, acceptedFiles } = useDropzone({ noClick: true, noKeyboard: true });

  return (
    <Fragment>
      <MuiPickersUtilsProvider utils={MomentUtils} locale="fr">
        <section>
          <DragDropFiles files={acceptedFiles} onSubmit={handleRefresh} />
          <div {...getRootProps({ className: 'dropzone' })}>
            <div {...getInputProps()} />
            {tab === "home" && <DailyEntry shouldRefresh={refresh} onTabChange={handleTabChange} />}
            {tab === "extraction" && <Extraction shouldRefresh={refresh} onTabChange={handleTabChange} />}
            {tab === "gestionemployee" && <GestionEmployee shouldRefresh={refresh} onTabChange={handleTabChange} />}
          </div>
        </section>
      </MuiPickersUtilsProvider>

    </Fragment>
  );
}

export default App;
