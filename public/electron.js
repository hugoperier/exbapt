const { app, BrowserWindow } = require("electron");
const employeeDb = require("./db/stores/employee");
const employeeEntryDb = require("./db/stores/employeeEntry");
const notes = require("./db/stores/notes");
const rttPaid = require("./db/stores/rttPaid");

const path = require("path");
const isDev = !app.isPackaged;

global.employeeDb = employeeDb;
global.employeeEntryDb = employeeEntryDb;
global.notesDb = notes;
global.rttPaidDb = rttPaid;

let win;

function createWindow() {
  win = new BrowserWindow({
    width: 1540,
    height: 880,
    autoHideMenuBar: true,
    transparent: false,
    webPreferences: {
      // <--- (1) Additional preferences
      nodeIntegration: true,
      preload: __dirname + "/preload.js", // <--- (2) Preload script
    },
  });
  //win.loadURL("http://localhost:3000"); // <--- (3) Loading react
  win.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "../build/index.html")}`
  );

  if (isDev) {
    win.webContents.openDevTools();
  }

  win.on("closed", () => {
    win = null;
  });
}

app.on("ready", createWindow);
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});
app.on("activate", () => {
  if (win === null) {
    createWindow();
  }
});
