const Datastore = require("nedb-promises");
const Ajv = require("ajv");
const todoItemSchema = require("../schemas/employee");

class EmployeeStore {
  constructor() {
    const ajv = new Ajv({
      allErrors: true,
      useDefaults: true,
    });

    this.schemaValidator = ajv.compile(todoItemSchema);
    const dbPath = `${process.cwd()}/employee.db`;
    this.db = Datastore.create({
      filename: dbPath,
      timestampData: true,
    });
  }

  validate(data) {
    return this.schemaValidator(data);
  }

  async create(data) {
    const isValid = this.validate(data);
    if (isValid) {
      return await this.db.insert(data);
    }
  }

  update(record) {
    this.db.update(
      { _id: record.id },
      {
        $set: {
          firstName: record.firstName,
          lastName: record.lastName,
          isActive: record.isActive,
          baseRtt: record.baseRtt,
          address: record.address,
          phone: record.phone,
          email: record.email
        },
      },
      {}
    );
  }

  getByNames(firstName, lastName) {
    return this.db.findOne({ firstName, lastName }).exec();
  }

  read(_id) {
    return this.db.findOne({ _id }).exec();
  }

  readAll() {
    return this.db.find();
  }

  readActive() {
    return this.db.find({ isDone: false }).exec();
  }
}

module.exports = new EmployeeStore();
