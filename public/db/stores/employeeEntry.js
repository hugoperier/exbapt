const Datastore = require("nedb-promises");
const Ajv = require("ajv");
const employeeEntrySchema = require("../schemas/employeeEntry");

class EmployeeEntryStore {
  constructor() {
    const ajv = new Ajv({
      allErrors: true,
      useDefaults: true,
    });

    this.schemaValidator = ajv.compile(employeeEntrySchema);
    const dbPath = `${process.cwd()}/entry.db`;
    this.db = Datastore.create({
      filename: dbPath,
      timestampData: true,
    });
  }

  validate(data) {
    if (data !== undefined) delete data.id;
    return this.schemaValidator(data);
  }

  async create(data) {
    const isValide = this.validate(data);

    if (isValide) {
      if (data._id) delete data._id;
      const insert = await this.db.insert(data);
      return insert;
    }
  }

  delete(employeeEntryId) {
    this.db.remove({ _id: employeeEntryId }, {});
  }

  update(employeeEntryId, data) {
    this.db.update(
      { _id: employeeEntryId },
      {
        $set: {
          employeeId: data.employeeId,
          hoursWorked: data.hoursWorked,
          mg: data.mg,
          worksiteMeal: data.worksiteMeal,
          restaurant: data.restaurant,
          rttTooken: data.rttTooken,
          rttCum: data.rttCum,
          absenceType: data.absenceType,
          absenceHours: data.absenceHours,
          publicHoliday: data.publicHoliday,
        },
      },
      {}
    );
  }

  /*
   * Get all the record of the day for every employee
   */
  getDailyRecord(date) {
    const day = new Date(date);
    const before = new Date(
      day.getFullYear(),
      day.getMonth(),
      day.getDate(),
      0
    );
    const after = new Date(
      day.getFullYear(),
      day.getMonth(),
      day.getDate(),
      23,
      59
    );
    return this.db.find({ date: { $gte: before, $lt: after } });
  }

  getRecords() {
    return this.db.find();
  }
}

module.exports = new EmployeeEntryStore();
