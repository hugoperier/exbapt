const Datastore = require("nedb-promises");
const Ajv = require("ajv");
const noteSchema = require("../schemas/notes");

class NoteStore {
  constructor() {
    const ajv = new Ajv({
      allErrors: true,
      useDefaults: true,
    });

    this.schemaValidator = ajv.compile(noteSchema);
    const dbPath = `${process.cwd()}/notes.db`;
    this.db = Datastore.create({
      filename: dbPath,
      timestampData: true,
    });
  }

  validate(data) {
    return this.schemaValidator(data);
  }

  async create(data) {
    const isValid = this.validate(data);

    if (isValid) {
      const insert = await this.db.insert(data);
      return insert;
    }
  }

  delete(noteId) {
    this.db.remove({ _id: noteId }, {});
  }

  update(noteId, data) {
    this.db.update(
      { _id: noteId },
      {
        $set: {
          content: data.content,
          date: data.date
        },
      },
      {}
    );
  }

  getAll() {
    return this.db.find();
  }
}

module.exports = new NoteStore();
