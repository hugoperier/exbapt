const Datastore = require("nedb-promises");
const Ajv = require("ajv");
const rttPaidSchema = require("../schemas/rttPaid");

class RTTPaidStore {
  constructor() {
    const ajv = new Ajv({
      allErrors: true,
      useDefaults: true,
    });

    this.schemaValidator = ajv.compile(rttPaidSchema);
    const dbPath = `${process.cwd()}/rttpaid.db`;
    this.db = Datastore.create({
      filename: dbPath,
      timestampData: true,
    });
  }

  validate(data) {
      if (data !== undefined) delete data.id
      return this.schemaValidator(data)
  }

  async create(data) {
      const isValid = this.validate(data)

      if (isValid) {
          const insert = this.db.insert(data)
          return insert
      }
  }

  delete(rttPaidId) {
      this.db.remove({_id: rttPaidId}, {});
  }

  getAll() {
      return this.db.find()
  }
}

module.exports = new RTTPaidStore()