const employeeEntrySchema = {
  type: "object",
  properties: {
    employeeId: {
      type: "string",
    },
    hoursWorked: {
      type: "number",
    },
    mg: {
      type: "integer",
    },
    worksiteMeal: {
      type: "boolean",
      default: "false",
    },
    restaurant: {
      type: "boolean",
      default: "false",
    },
    absenceType: {
      type: "string",
      default: null
    },
    absenceHours: {
      type: "number",
      default: 0
    },
    date: {
      type: "object",
    },
  },
};

module.exports = employeeEntrySchema;
