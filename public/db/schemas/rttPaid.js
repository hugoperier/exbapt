const rttPaidSchema = {
  type: "object",
  properties: {
    employeeId: {
      type: "string",
    },
    rttPaid: {
      type: "number",
    },
    date: {
      type: "object",
    },
  },
};

module.exports = rttPaidSchema;
