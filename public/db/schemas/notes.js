const noteSchema = {
  type: "object",
  properties: {
    content: {
      type: "string",
      default: "",
    },
    date: {
      type: "object",
    },
  },
};

module.exports = noteSchema;
