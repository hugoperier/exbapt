const employeeSchema = {
    type: 'object',
    properties: {
      firstName: {
        type: 'string',
      },
      lastName: {
        type: 'string',
      },
      address: {
        type: 'string',
        default: ""
      },
      phone: {
        type: 'string',
        default: ""
      },
      email: {
        type: 'string',
        default: ""
      },
      isActive: {
        type: 'boolean',
        default: false
      },
      baseRtt: {
        type: "number",
        default: 0
      }
    },
  };
  
  module.exports = employeeSchema;
  